\section{Theoretische inleiding}\label{sec:theoretische-inleiding}\todo{Betere naam: achtergrond? \emph{preliminaries}?}

Het paradigma van functionele programmeertalen heeft zijn oorsprong in de \lamcalc{}.
Om SSML en de SKI-combinatorcalculus te begrijpen, is daarom kennis van de \lamcalc{} nodig.
De ongetypeerde, de simpel getypeerde en de polymorfische \lamcalc{} zullen in dit hoofdstuk besproken worden.
Daarnaast zal de SKI-combinatorcalculus en het verband met de ongetypeerde \lamcalc{} besproken.

\subsection{De ongetypeerde lambda-calculus}
\todo{Een (beknopte) beschrijving van de lambdacalculus}

\subsubsection{Syntaxis}
De \lamcalc{} heeft drie verschillende soorten termen:
\begin{align*}
  t ::=\ & x            & \text{(variabele)} \\
      |\ & \lambda x. t & \text{(abstractie)} \\
      |\ & t\ t         & \text{(applicatie)}
\end{align*}
Hiervoor geldt dat \(x\) voor een (willekeurige) variabele staat.
Voor de namen van variabelen worden vaak (Latijnse) letters of woorden gebruikt.

\(\lambda\)-abstracties kunnen gezien worden als afbeeldingen;
de term \(\lambda x. t\) is dan de afbeelding \(f\) gegeven door \(f(x) = t\)\todo{\(t(x)\) of gewoon \(t\)?}.
\(\lambda\)-abstracties in de vorm \(\lambda x. t\) \emph{binden} de variabele \(x\);
telkens wanneer \(x\) voorkomt in \(t\) is \(x\) \emph{gebonden}.
De niet-gebonden variabelen in een term noemt men de \emph{vrije variabelen} (Engels: \emph{free variables}).
Zo wordt de verzameling van vrije variabelen in een term gegeven door:
\begin{align*}
  FV(x) & = \{x\} \\
  FV(\lambda x. t) & = FV(t) \setminus \{x\} \\
  FV(t_1\ t_2) & = FV(t_1) \cup FV(t_2)
\end{align*}

De applicatie van termen kan vergeleken worden met het toepassen van afbeeldingen op argumenten.
Om de applicatie uit te voeren moeten eerst \emph{substituties} bestudeerd worden.
Een substitutie vervangt de variabelen in een term door andere termen.
De substitutie waarin alle (vrije) variabelen \(x\) in de term \(t\) vervangen worden door de term \(s\) wordt geschreven als \([x |-> s] t\).
Substitutie wordt als volgt gedefinieerd:
\begin{equation*}
\begin{array}{l l r}
  %% NOOT: de blokhaken moeten binnen accolades, omdat ze anders door LaTeX
  %% worden gezien als parameters.
  {[x |-> s]}x & = s & \\
  {[x |-> s]}y & = y & \text{als } x \neq y \\
  {[x |-> s]}(\lambda y. t) & = \lambda y.\ [x |-> s]t & \text{als } x \neq y \text{ en } y \not\in FV(s) \\
  {[x |-> s]}(t_1\ t_2) & = [x |-> s]t_1 [x |-> s]t_2 &
\end{array}
\end{equation*}
Merk op dat substitutie in deze definitie een partiële functie is: als \(y \in FV(s)\) dan \([x |-> s](\lambda y. t)\) geen waarde hebben.
Dit is het geval, omdat de \(y\) in \(s\) gebonden zou woorden door de \(\lambda\)-abstractie.
Om dit op te lossen kan \(y\) door een nog niet gebonden variabele vervangen zou worden;
dit noemt men \(\alpha\)-conversie.
Er wordt daarom gesproken van een substitutie `tot \(\alpha\)-conversie'\todo{is tot de beste vertaling van up to?};
zie voor een uitgebreidere beschrijving en uitleg \cite[pp. 69--71]{Pierce02}.
Om dit probleem op een geraffineerder wijze op te lossen, kan bijvoorbeeld gebruikt gemaakt worden van \emph{De Bruijn-indices}, een representatie van termen die geen gebruik maakt van variabelen met namen, maar van getallen die verwijzen naar de \(\lambda\)-abstractie waarin de variabele gebonden is (\cite{DeBruijn72}; \cite[hoofdstuk 6]{Pierce02}).

\subsubsection{Semantiek}
Met de definitie van substituties kan de uitvoerstrategie gedefinieerd worden aan de hand van \emph{operationele semantiek}\todo{bron?}.
De evaluatieregels zijn als volgt, waarbij gebruik wordt gemaakt van een \emph{natuurlijke}\todo{juiste vertaling van natural in deze context?} ofwel \emph{big step} semantiek:
\begin{equation*}
  \inference[\textsc{E-Abs}: ]{}{
    \lambda x. t \Downarrow \lambda x. t
  }
\end{equation*}
\begin{equation*}
  \inference[\textsc{E-App}: ]{
      t_1 \Downarrow \lambda x. t_1' & t_2 \Downarrow t_2' & [x |-> t_2']t_1' \Downarrow t'
  }{
    t_1\ t_2 \Downarrow t'
  }
\end{equation*}

\subsubsection{De Church-codering}\label{sec:church}
In de \lamcalc{} kunnen waarden als (natuurlijke) getallen en booleaanse waarden zonder veel moeite geïmplementeerd worden.
Zo kunnen de booleaanse waarden worden als volgt geschreven:
\begin{align*}
  \lamTrue & = \lambda t.\ \lambda f.\ t \\
  \lamFalse & = \lambda t.\ \lambda f.\ f
\end{align*}
Een \textbf{if}-\textbf{then}-\textbf{else}-functie kan dan zo geïmplementeerd worden:
\begin{equation*}
  \mathsf{ifThenElse} = \lambda i.\ \lambda t.\ \lambda e.\ i\ t\ e
\end{equation*}
(Merk op dat \(\mathsf{ifThenElse}\ i\ t\ e === i\ t\ e\).)

Getallen worden gecodeerd door middel van het herhaaldelijk toepassen van functies.
De definities van \(0\) en \(\mathsf{succ}\) (voor \emph{succesor}, opvolger), de functie die \(1\) optelt bij haar argument, zijn:
\begin{align*}
  0 & = \lambda f.\ \lambda x.\ x \\
  \mathsf{succ} & = \lambda n.\ \lambda f.\ \lambda x.\ f\ (n\ f\ x)
\end{align*}
Voor een natuurlijk getal \(n\) geldt dat de codering in de \lamcalc{} \(\lambda f.\ \lambda x.\ f^{(n)}\ x\) is (\cite[p. 13]{Barendregt00});
hier is \(f^{(k)}\) voor een natuurlijk getal \(k\) de functie die \(f\) precies \(k\) keer toepast op haar argument.

Zie \cite[pp. 58--63]{Pierce02} voor de definities van bewerkingen als logische conjunctie en disjunctie, optelling, vermenigvuldiging, etc. voor de Church-codering.

\subsection{De simpel getypeerde lambda-calculus}
De simpel getypeerde \lamcalc{} is een uitbreiding van de ongetypeerde \lamcalc{}, bijvoorbeeld door de toevoeging van getallen of booleaanse waarden.
In de rest van deze paragraaf zullen die waarden met respectievelijk de naam \(\lamInt\) en \(\lamBool\) toegevoegd worden.
De syntaxis en typeringsregels worden bijgewerkt om voor de simpel getypeerde \lamcalc{} juist te zijn.

\subsubsection{Syntaxis}
Naast termen heeft de simpel getypeerde \lamcalc{} ook typen:
\begin{align*}
  \tau ::=\ & \lamBool & \text{(type van booleans)} \\
         |\ & \lamInt  & \text{(type van getallen)} \\
         |\ & \tau -> \tau  & \text{(type van functies)}
\end{align*}
De definitie van termen verschilt ook, door de toevoeging van getal- en booleaanse waarden, en in de syntaxis voor \(\lambda\)-abstracties.
Bovendien wordt een \textbf{if}-\textbf{then}-\textbf{else}-term toegevoegd, die het toevoegen van booleans enigszins nuttig maakt.
Voor getallen wordt alleen de optellingsbewerking toegevoegd;
de syntaxis, typering en semantiek voor andere bewerkingen op getallen lijken sterk op die van optelling.
\begin{align*}
  t ::=\ & \lamTrue\ |\ \lamFalse & \text{(booleaanse waarden)} \\
      |\ & n & \text{als } n \in \mathbb{N} \text{ (getallen)} \\
      |\ & x & \text{(variabele)} \\
      |\ & \lambda x:\tau.\ t & \text{(abstractie)} \\
      |\ & t\ t & \text{(applicatie)} \\
      |\ & \lamIfThenElse{t}{t}{t} & \text{(voorwaarde)} \\
      |\ & t + t & \text{(optelling)}
\end{align*}

\subsubsection{Typering}
De typeringsregels voor de simpel getypeerde \lamcalc{} zijn als volgt:
\todo{Uitleggen hoe zulke regels werken.}
\begin{equation*}
  \inference[\textsc{T-True}: ]{}{
    \Gamma |- \lamTrue : \lamBool
  }
\end{equation*}
\begin{equation*}
  \inference[\textsc{T-False}: ]{}{
    \Gamma |- \lamFalse : \lamBool
  }
\end{equation*}
\begin{equation*}
  \inference[\textsc{T-Int}: ]{
    n \in \mathbb{N}
  }{
    \Gamma |- n : \lamInt
  }
\end{equation*}
\begin{equation*}
  \inference[\textsc{T-Var}: ]{
    x : \tau \in \Gamma
  }{
    \Gamma |- x : \tau
  }
\end{equation*}
\begin{equation*}
  \inference[\textsc{T-Abs}: ]{
    \Gamma, x : \tau_1 |- t_2 : \tau_2
  }{
    \Gamma |- \lambda x:\tau_1.\ t_2 : \tau_1 -> \tau_2
  }
\end{equation*}
\begin{equation*}
  \inference[\textsc{T-App}: ]{
    \Gamma |- t_1 : \tau_{1, 1} -> \tau_{1, 2} & \Gamma |- t_2 : \tau_{1, 1}
  }{
    \Gamma |- t_1\ t_2 : \tau_{1, 2}
  }
\end{equation*}
\begin{equation*}
  \inference[\textsc{T-Plus}: ]{
    \Gamma |- t_1 : \lamBool & \Gamma |- t_2 : \tau & \Gamma |- t_3 : \tau
  }{
    \Gamma |- \mathsf{if}\ t_1\ \mathsf{then}\ t_2\ \mathsf{else}\ t_3 : \tau
  }
\end{equation*}
\begin{equation*}
  \inference[\textsc{T-Plus}: ]{
    \Gamma |- t_1 : \lamInt & \Gamma |- t_2 : \lamInt
  }{
    \Gamma |- t_1 + t_2 : \lamInt
  }
\end{equation*}
\todo{(Misschien) semantiek.}

\subsection{SKI-combinatorcalculus}
De SKI-combinatorcalculus is een systeem van combinatorische logica dat berust op slechts drie bouwstenen (terug te brengen tot twee, waarover later meer).
Deze bouwstenen zijn de \(\skiS\)-, \(\skiK\)-, en \(\skiI\)-combinatoren, gegeven in de volgende vergelijkingen (\cite{Turner79}; \cite[p. 12]{Barendregt00}):
\begin{align}
  \skiS\ f\ g\ x & = f\ x\ (g\ x) \tag{S}\label{eqn:S} \\
  \skiK\ x\ y & = x \tag{K}\label{eqn:K} \\
  \skiI\ x & = x \tag{I}\label{eqn:I}
\end{align}
Deze combinatoren nemen een aantal argumenten en geven daarna iets terug, wat de \emph{reductie} van de combinator genoemd wordt.

\subsubsection{Abstractie}
Elke term van de \lamcalc{} zonder vrije variabelen kan in SKI-combinatoren gecodeerd worden (\cite{Turner79}).
Deze vertaling wordt \emph{abstractie} genoemd.\todo{abstractie definiëren}

\todo{Volgende alinea moet m.i. niet in de theoretische inleiding besproken worden, maar in de paragraaf `SKI'.}
We kunnen voor uitvoering ook nog een aantal functies declareren om het sneller te maken zoals optellen (add) en aftrekken (sub).
Dat levert expressies als \(\skiS (\skiK \skifn{add})(\skiK 1) 4 = 5\) op.

\subsubsection{Reductie}
Hieronder is de reductie van de termen \(\skiS \skiK \skiK x\) en \(\skiI x\) gegeven, waarin \(x\) eender welke term voorstelt.
Aangezien de producten van beide reducties gelijk zijn aan \(x\), kan geconcludeerd worden dat \(\skiS \skiK \skiK === \skiI\).
De combinator \(\skiI\) is dus overbodig, maar omdat de notatie compacter en duidelijker is, zal in het vervolg gebruik worden gemaakt van \(\skiI\).
\begin{align*}
     &\ \skiS \skiK \skiK x & \\
  => &\ \skiK x (\skiK x) & \text{(per definitie \ref{eqn:S})} \\
  => &\ x & \text{(per definitie \ref{eqn:K})} \\
     & & \\
     &\ \skiI x & \\
  => &\ x & \text{(per definitie \ref{eqn:I})}
\end{align*}

\subsection{De polymorfe lambda-calculus}\todo{polymorfisch of polymorf?}
\subsubsection{Motivatie}
De simpel getypeerde \lamcalc{} heeft een voordeel over de ongetypeerde \lamcalc{}: de juistheid van een programma kan (tot op zekere hoogte) statisch (vóór het uitvoeren) gecontroleerd worden.
Een nadeel is echter, dat deze typering zorgt voor duplicatie;
waar in de ongetypeerde \lamcalc{} nog \(\lambda x. x\) gebruikt kan worden als identiteitsfunctie, moet in de simpel getypeerde \lamcalc{} voor elk type een andere identiteitsfunctie geschreven worden.
Dat levert definities als de volgende op:
\begin{align*}
  \mathsf{id}_{\lamInt} & = \lambda x : \lamInt.\ x \\
  \mathsf{id}_{\lamBool} & = \lambda x : \lamBool.\ x \\
  \mathsf{id}_{\lamInt -> \lamInt} & = \lambda x : \lamInt -> \lamInt.\ x \\
  \mathsf{id}_{\lamBool -> \lamInt} & = \lambda x : \lamBool -> \lamInt.\ x \\
  & \vdots
\end{align*}
Dit gaat in tegen het \emph{abstractieprincipe}:
\begin{quote}\textquote{%
  \textsc{Abstraction principle}: Each significant piece of functionality in a program should be implemented in just one place in the source code.
  Where similar functions are carried out by distinct pieces of code, it is generally beneficial to combine them into one by abstracting out the varying parts.} \\
  (\cite[p. 339]{Pierce02})
\end{quote}

De oplossing voor het hierboven beschreven probleem is de \emph{polymorfe \lamcalc{}}.

\subsubsection{Syntaxis}
De syntaxis van typen in de polymorfe \lamcalc{} bevat ten opzichte van de simpel getypeerde \lamcalc{} één toevoeging: typevariabelen.
De regels zijn zo, waar \(\alpha\) een willekeurige typevariabele voorstelt:
\begin{align*}
  \tau ::=\ & \lamBool      & \text{(type van booleans)} \\
         |\ & \lamInt       & \text{(type van getallen)} \\
         |\ & \tau -> \tau  & \text{(type van functies)} \\
         |\ & \alpha        & \text{(typevariabele)}
\end{align*}
Ook de syntaxis van termen bevat een aanpassing;
de \(\lambda\)-abstracties bevatten geen expliciete typen meer, zodat de termen volledig impliciet getypeerd zijn:
\begin{align*}
  t ::=\ & \lamTrue\ |\ \lamFalse & \text{(booleaanse waarden)} \\
      |\ & n & \text{als } n \in \mathbb{N} \text{ (getallen)} \\
      |\ & x & \text{(variabele)} \\
      |\ & \lambda x.\ t & \text{(abstractie)} \\
      |\ & t\ t & \text{(applicatie)} \\
      |\ & \lamIfThenElse{t}{t}{t} & \text{(voorwaarde)} \\
      |\ & t + t & \text{(optelling)}
\end{align*}
Er bestaan in de polymorfe \lamcalc{}, naast typen, ook \emph{typeschema's}.
De syntaxis van typeschema's is als volgt:
\begin{equation*}
  \sigma ::= \forall \vec{\alpha}.\ \tau
\end{equation*}
Hierin is \(\vec{\alpha}\) een (mogelijk lege) lijst van typevariabelen.
\todo{Uitleggen wat een typeschema inhoudt}

De regel voor typeschema's zou ook in de regels voor typen opgenomen kunnen worden;
die taal wordt \emph{System F} genoemd, waarin een type als \(\forall \alpha.\ (\forall \beta.\ \beta -> \beta) -> \alpha -> \alpha\) syntactisch correct is (en er bestaat ook een term met dat type).
Omdat typereconstructie voor System F onbeslisbaar is, en omdat de taal SSML wel gebruik maakt van typereconstructie, is er hier gekozen om typen en typeschema's gescheiden te houden.
Het typesysteem dat de typering van deze taal beschrijft, is het \emph{Hindley-Damas-Milner-typesysteem} (\cite{Damas82}).

Deze \lamcalc{} lost het hierboven beschreven probleem op: er bestaat een universele identiteitsfunctie, geschreven als
\[
  \mathsf{id} = \lambda x.\ x \text{,}
\]
met het typeschema
\[
  \forall \alpha.\ \alpha -> \alpha \text{,}
\]
die op alle waarden toegepast kan worden.

\subsubsection{Typering}
\todo{Toevoegen}
