# Gewenste operaties voor een Rust-achtige vector
- indexering (ook met waarden van runtime)
- insert (ook met waarden van runtime)
- delete (ook met waarden van runtime)

# Vector in brainfuck

## Datastructuur
| Begin | Element x | Element y | End |
| ----- | --------- | --------- | --- |
| 0     | 1 x       | 1 y       | 0   |

## Terminologie
Ik bedenk nu pas dat ik misschien een paar concepten een naam moet geven,
deze namen zijn niet consequent in dit document gebruikt maar kunnen wel conversaties vereenvoudigen.

| Naam | Uitleg |
| ---- | ------ |
| Element | Een waarde die bij een index hoort, deze waarde kan uit meerdere cellen bestaan: de "grootte" van een element. |
| Hoofd | Een cel die voorafgaat aan een element, dit kan een 1, 0, 255 of misschien ook een andere waarde hebben. Deze waarde geeft de "staat" van een element aan. |
| Staat van een element | Een element kan in gebruik (1), geselecteerd (0) of gealloceerd maar ongebruikt (255) zijn. |

## Traversal
Ga naar begin.
```brainfuck
>[
    sla de 1 over die bij elk element staat
    >

    doe iets met huidige element, min 1 bijvoorbeeld
    -

    ga naar volgende element, grootte 1 als voorbeeld
    >
]
```
Je bent nu bij het einde.

### Multidimensional arrays
Als een element ook een array is dan wordt de stap "ga naar volgende element" vervangen door nog een traversal: `>[>>]>`.

### O(log(n)) traversal
Hiervoor moeten we de grootte van een stap coderen per element.
Stel dat we 16 bit indexering hebben, dan bewegen we steeds 256 elementen.
Elk element waar zo'n stap begon moet gemerkt worden en dan kunnen we bij de traversal ook zo'n stap van 256 elementen zetten.

## Selecteren
We kunnen 1 element in de array "selecteren" door de 1 voor het element naar een 0 te veranderen. Hierdoor stopt de traversal daar en kunnen we iets met het element doen. Daarna gaan we verder met de traversal om bij het echte einde te komen. Dit werkt zolang er precies 1 element "geselecteerd" is.

### Vind geselecteerde element
Ga naar begin.
```brainfuck
>[
    sla de 1 over die bij elk element staat
    >

    ga naar volgende element, grootte 1 als voorbeeld
    >
]
```
Nu bevind je je op de 0 voor het geselecteerde element.

### Selecteer volgende element
Vind geselecteerde element.
```brainfuck
deselecteer en sla de 1 over
+>

ga naar volgende element, grootte 1 als voorbeeld
>

selecteer dit element
-
```
Nu bevind je je op de 0 voor het geselecteerde element.

### Selecteer eerste element
Ga naar begin.
```brainfuck
>-
```
Nu bevind je je op de 0 voor het geselecteerde en eerste element.

## Indexering
We moeten het element met de index van cel N selecteren.

Selecteer eerste element
Ga naar N.
```brainfuck
[
    decr N
    -

    Selecteer volgende element
    (niet ingevuld hier)

    Ga naar N
    (niet ingevuld hier)
]
```

### 16 bit indexering
Hierbij ga ik uit van twee cellen: M en N, waarbij `getal = M * 256 + N`.

Ga naar N.
Indexeer.
Ga naar M.
Indexeer maar selecteer nu elke loop het 256e volgende element.

## Getting and setting
Als je iets geselecteerd hebt wil je er ook iets mee doen.

### Getting
Dit is niet anders dan `getting` voor een ander element eerder op de stack, uitbreiden als de grootte van het element meer dan 1 is.
```brainfuck
push cel
>

Ga naar geselecteerd element.
(niet ingevuld hier)

>[
    decr
    -

    Ga naar %sp
    (niet ingevuld hier)

    incr $sp
    +

    incr $sp1
    >+<

    Ga naar geselecteerd element
    (niet ingevuld hier)

    >
]

Ga naar %sp
(niet ingevuld hier)

verplaats $sp1 weer terug
>[
    decr
    -

    Ga naar geslecteerd element
    (niet ingevuld hier)

    incr
    >+

    Ga naar %sp
    (niet ingevuld hier)

    >
]<

Ga naar geselecteerd element
(niet ingevuld hier)

Ga naar de 0 voor geselecteerd element
<

Ga naar %sp
```

### Setting
Ook dit is niet anders dan `setting` voor een eerder op de stack geplaatste waarde en ook dit moet uitgebreid worden voor elementen die groter dan 1 cel zijn.

Ik ben nu te lui om dit helemaal op te schrijven dus zeg ik gewoon dat het triviaal is.

## Datastructuur met ongebruikte cellen
We gebruiken cellen met 255 i.p.v. 1 om aan te geven dat het element ongebruikt is en dus gevuld kan worden bij operaties als `push` en `pop`.

| Begin | Element x | Element y | Empty | Empty | End |
| ----- | --------- | --------- | ----- | ----- | --- |
| 0     | 1 x       | 1 y       | 255 0 | 255 0 | 0   |

### Selecteer eerste ongebruikte cel
Ga naar begin.
```brainfuck
Ga naar eerste element
>

incr (reden verderop uitgelegd)
+

[
    decr want dit was een 1 en dus al gebruikt
    -

    Ga naar volgende element
    (hier niet ingevuld)

    incr, als dit 255 is dan wordt het 0 en stopt de loop
    +
]
```
Nu hebben we de eerste cel met 255 geselecteerd.

#### Van de andere kant.
Het is meestal efficiënter om dit vanaf de andere kant te doen, gelukkig is hetzelfde principe te gebruiken.

Ga naar einde.
```brainfuck
Ga naar vorige element
(hier niet ingevuld)

- decr

[
    incr want dit was een 255 dus die slaan we over
    +

    Ga naar vorige element
    (hier niet ingevuld)

    decr
    -
]

Nu hebben we het laatste element met 1 geselecteerd.

Met het volgende stuk kunnen we het eerste element met 255 selecteren.

+ (van 0 naar 1)

Ga naar volgend element
(hier niet ingevuld)

+ (van 255 naar 0)
```

## Push en pop met vaste capacity
Nu we een verschil tussen `size` en `capacity` hebben kunnen we gaan beginnen met dynamische grootte,
allereerst definiëren we `push` en `pop` omdat dat makkelijker is dan `insert` en `delete`.

### Push
Ga naar begin.
Selecteer eerste ongebruikte element.
Set naar geselecteerd element.

### Pop
Ga naar begin.
Selecteer laatste gebruikte element (zie "Van de andere kant")
Ga naar geselecteerd element
```brainfuck
verander 1 naar 255
--

zet alle cellen van element naar 0, in voorbeeld slechts 1 cel
>[-]
```

## Insert en delete met vaste capacity
TODO

### Insert
We hebben een cel N met de index en element E wat geïnsert moet worden.
Dit gaat ervan uit dat er ten minste 1 ongebruikt element is.

Indexeer N (let op, deze is dus geselecteerd).
```brainfuck
Ga naar eerste ongebruikte element.
(niet ingevuld hier)

verander 255 naar 1
++

Ga naar vorige element
(niet ingevuld hier)
[
    skip de 1 aan het begin
    >
    verplaats naar volgende element, voorbeeld met grootte 1
    [-
        Ga naar volgende element
        +
        Ga terug
    ]

    Ga terug naar de 1 aan het begin
    <

    Ga naar vorige element
    (niet ingevuld hier)
]

Nu zijn alle elementen boven ons geselecteerde element verplaatst.

Verplaats geselecteerd element
(niet ingevuld, zie code voor verplaatsen hierboven)

Set naar geselecteerd element
(niet ingevuld)
```

### Delete
We hebben een cel N met de index van het element wat weg moet.

Indexeer N (let op, deze is dus geselecteerd).
```brainfuck
Ga naar geselecteerd element

Deselecteer element
+

Leeg huidig element, grootte 1 bij voorbeeld
>[-]

Ga naar volgend element
>

incr
+

[
    decr want dit was geen 255
    -

    Verplaats naar vorige element, voorbeeld met grootte 1
    [
        -

        ga naar vorige element
        (niet ingevuld hier)

        +

        ga terug
        (niet ingevuld hier)
    ]

    Ga naar volgend element
    >
]

We zitten nu bij een cel die 255 was

Verander naar 255
-

Ga naar vorige element
(niet ingevuld hier)

Verander ook naar 255 (vanaf 1) want deze hebben we net verplaatst
--

Ga terug naar einde

Ga naar volgend element
(niet ingevuld hier)

[
    Ga naar volgend element
    (niet ingevuld hier)
]
```

## Capacity vergroten
Ok, we willen `N` extra elementen kwijt kunnen die lengte `L` hebben, de totale lengte van een element is dus `L+1` omdat er ook nog een 255 of een 1 voor moet.
Om dit te bereiken moeten alle elementen die hiervoor op de stack zitten `N*(L+1)` cellen opschuiven (achteraan beginnend):
```brainfuck
[
    decr
    -

    Ga N*(L+1) cellen naar rechts

    incr
    +

    Ga N*(L+1) cellen naar links
]
```
Dit kunnen we voor elke cel doen aangezien de compiler weet hoeveel dat er zijn.

Er kan wel nog een vector op de stack zitten, om die te verplaatsen kunnen we dit gebruiken (let op, we beginnen achteraan met verplaatsen):
```brainfuck
Ga naar laatste element

[
    Ga naar einde element

    Verplaats alle onderdelen van het element, in het voorbeeld grootte 2

    Verplaats dit (zie snippet hierboven)
    <
    Verplaats dit (zie snippet hierboven)

    Ga naar 1 of 255 voor element
    <

    incr om 255 naar 0 te halen (sneller bij kopiëren)
    +

    Verplaats dit (zie snippet hierboven)

    Ga N*(L+1) cellen naar rechts
    -
    Ga N*(L+1) cellen naar links

    Ga naar vorig element
]

Nu zijn alle elementen opgeschoven, het begin en eind zijn ook "opgeschoven" aangezien ze 0 zijn.
We bevinden ons nu op de positie van het oude begin.
```

Goed, nu hebben we alles boven onze vector opgeschoven en kunnen we hem uitbreiden,
ik ga ervan uit dat we nu bij het nieuwe einde zijn.
Voer het volgende stuk code N keer uit
```brainfuck
Ga naar vorig element
(L+1)<

zet naar 255
-
```

## Time complexity
Ik ga er hierbij van uit dat instructies gecombineerd worden bij het interpreteren/compilen, hiermee bedoel ik dat `>>>` (`p += 3`) even lang duurt als `>` (`p += 1`).

Traversal is `O(n)` en indexeren `O(n * log(index))` als we vanaf het begin indexeren (`arr[0]` zit aan het begin), dit is een beetje jammer omdat een loop hierdoor `O(n^2 * log(n))` wordt. We zouden ook vanaf het einde kunnen indexeren (`arr[0]` zit aan het eind) en dan zou indexeren `O(log(index))` kunnen zijn als we het slim doen (zie "O(log(n)) traversal"); een loop wordt dan `O(n * log(n))`. Dit is echter wel weer lastiger om te programmeren.

Het is echter interessant dat we bij de traversal alle elementen tegen komen: hét kenmerk van een loop!
Hier kan vast nog iets slims mee gedaan worden, ik zie alleen nog niet in hoe aangezien operaties dingen op de stack pushen en dat jammer genoeg niet kan.

Disclaimer: mijn hoofd doet pijn terwijl ik dit allemaal beredeneer en dus is er een behoorlijke kans op fouten.

# Brainfuck assembly
Uiteindelijk moeten we instructies definiëren, dit is mijn voorstel.
Al deze instructies hebben naast de parameters die erbij staan ook nog een "naam" als parameter (met als uitzondering `ARR_DESTROY`), net zoals `GET_VAR`, `SET_VAR` en `DECLARE_VAR`.

Algemeen:
- `ARR_MAKE element_size initial_capacity`
- `ARR_DESTROY`

Selectie:
- `ARR_INDEX $sp`
- `ARR_INDEX16 $sp-1 $sp`
- `ARR_INDEX32 $sp-2 $sp-1 $sp`
- `ARR_SEL_LAST`, selecteert het laatste element

Hier ben ik niet zeker over:
- `ARR_UNSEL`, deselecteert iets
- `ARR_SEL_NEXT`, selecteert het volgende element (UB als dat niet bestaat is makkelijk, het is echter denk ik ook mogelijk om een bool terug te geven over of de operatie gelukt is)
- `ARR_SEL_PREV`, selecteert het vorige element (zie `ARR_SEL_NEXT` voor aantekeningen)

Manipulatie:
- `ARR_GET`
- `ARR_SET $sp-(n-1) $sp-(n-2) ... $sp`, for `element_size` n
- `ARR_PUSH $sp-(n-1) $sp-(n-2) ... $sp`, for `element_size` n
- `ARR_POP`
- `ARR_INSERT $sp-(n-1) $sp-(n-2) ... $sp`, for `element_size` n
- `ARR_DELETE`

Capaciteit:
- `ARR_ALLOCATE amount`

## Nadelen
Ik vind het onhandig dat de assembly fouten kan bevatten als men vergeet om `ARR_UNSEL` te gebruiken of het juist te vaak gebruikt maar anders moeten er zoveel verschillende vormen van `ARR_GET` zijn (8 bits, 16 bits, 32 bits, laatste element) dat dat ook weer niet goed gaat.

Wat misschien wel handig is is dat `ARR_UNSEL` automatisch wordt gedaan door `ARR_GET`, `ARR_SET`, etc.
Hierdoor worden `ARR_SEL_NEXT` en `ARR_SEL_PREV` onmogelijk maar ik weet ook niet of die echt handig zijn.
