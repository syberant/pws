mod parse;
mod ski;

fn main() {
    use std::io::Read;

    for filename in std::env::args().skip(1) {
        let path = std::path::Path::new(&filename);
        let mut file = std::fs::File::open(path).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents).unwrap();

        let parse_res = parse::parse_program_verbose(&contents);
        let program = parse::unwrap_verbose_error(&contents, parse_res);
        let result = program.execute();
        println!("{}: {}", path.display(), result);
    }
}
