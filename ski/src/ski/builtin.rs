use super::*;

#[derive(Clone, Debug, PartialEq)]
pub enum Value {
    Number(i64),
    Function(Function),
}

impl std::fmt::Display for Value {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use Value::*;
        let text = match self {
            Number(n) => n.to_string(),
            Function(n) => format!("{}", n),
        };

        write!(f, "{}", text)
    }
}

impl Value {
    fn unwrap_number(&self) -> i64 {
        if let Value::Number(n) = self {
            *n
        } else {
            panic!("Trying to unwrap a number but this ain't it...")
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum Function {
    Add,
    Sub,
    Mul,
    Div,
    Mod,
    Eq,
    Lt,
    Le,
    Gt,
    Ge,
}

impl std::fmt::Display for Function {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use Function::*;
        let text = match self {
            Add => "add",
            Sub => "sub",
            Mul => "mul",
            Div => "div",
            Mod => "mod",
            Eq => "eq",
            Lt => "lt",
            Le => "le",
            Gt => "ge",
            Ge => "Ge",
        };

        write!(f, "{}", text)
    }
}

impl Function {
    fn get_number(stack: &mut Vec<Element>) -> i64 {
        let elem = stack.pop().unwrap();
        let val = elem.0.unwrap_other();
        val.unwrap_number()
    }

    pub fn exec(&self, stack: &mut Vec<Element>) -> Element {
        use Function::*;

        let res = match self {
            Add => Function::two_nums_in(stack, |x, y| x + y),
            Sub => Function::two_nums_in(stack, |x, y| x - y),
            Mul => Function::two_nums_in(stack, |x, y| x * y),
            Div => Function::two_nums_in(stack, |x, y| x / y),
            Mod => Function::two_nums_in(stack, |x, y| x % y),
            Eq => Function::two_nums_in(stack, |x, y| x == y),
            Lt => Function::two_nums_in(stack, |x, y| x < y),
            Le => Function::two_nums_in(stack, |x, y| x <= y),
            Gt => Function::two_nums_in(stack, |x, y| x > y),
            Ge => Function::two_nums_in(stack, |x, y| x >= y),
        };

        // let res = Function::two_nums_in(stack, closure);

        Element::new(res)
    }

    fn two_nums_in<T: Into<Combinator>>(
        stack: &mut Vec<Element>,
        function: fn(i64, i64) -> T,
    ) -> Combinator {
        let x = Self::get_number(stack);
        let y = Self::get_number(stack);

        let res = function(x, y).into();

        res
    }
}