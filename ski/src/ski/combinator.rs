use super::{Element, Program, Value, Function};

#[derive(Clone, Debug, PartialEq)]
pub enum Combinator {
    S,
    K,
    I,
    Y,
    Multiple(Element, Element),
    Other(Value),
}

impl From<Value> for Combinator {
    fn from(v: Value) -> Combinator {
        Combinator::Other(v)
    }
}

impl From<Function> for Combinator {
    fn from(v: Function) -> Combinator {
        Value::Function(v).into()
    }
}

impl From<bool> for Combinator {
    fn from(b: bool) -> Combinator {
        use Combinator::*;

        if b {
            K
        } else {
            Combinator::mult(S, K)
        }
    }
}

impl From<i64> for Combinator {
    fn from(n: i64) -> Combinator {
        Combinator::Other(Value::Number(n))
    }
}

impl std::fmt::Display for Combinator {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use Combinator::*;
        let text = match self {
            S => "S".to_string(),
            K => "K".to_string(),
            I => "I".to_string(),
            Y => "Y".to_string(),
            Multiple(a, b) => format!("({}{})", a, b),
            Other(n) => format!("{}", n),
        };

        write!(f, "{}", text)
    }
}

impl Combinator {
    pub fn mult<A: Into<Element>, B: Into<Element>>(a: A, b: B) -> Combinator {
        Combinator::Multiple(a.into(), b.into())
    }

    pub fn unwrap_other(self) -> Value {
        match self {
            Combinator::Other(val) => val,
            Combinator::Multiple(a, b) => {
                let program = Program::new(vec![b, a]);
                program.execute().0.unwrap_other()
            }
            _ => panic!("Trying to unwrap to a `Value`, but this ain't it..."),
        }
    }

    pub fn step(mut stack: Vec<Element>) -> Vec<Element> {
        use Combinator::*;

        match *stack
            .pop()
            .expect("Tried to execute a step but there's nothing left on the stack")
            .0
        {
            S => {
                let f = stack.pop().unwrap();
                let g = stack.pop().unwrap();
                let x = stack.pop().unwrap();

                let fst = Element::new(Multiple(f, x.clone()));
                let snd = Element::new(Multiple(g, x));

                // let res = Element::new(Multiple(fst, snd));
                // stack.push(res);
                // The above gets unpacked by `Multiple` in the next step so just perform it now.

                stack.push(snd);
                stack.push(fst);
            }
            K => {
                let x = stack.pop().unwrap();
                let _y = stack.pop().unwrap();
                let res = x;
                stack.push(res);
            }
            I => {
                // let res = stack.pop().unwrap();
                // stack.push(res);
                // This means to do the above operations
                // But they accomplisch exactly nothing so I don't carry them out.
            }
            Y => {
                let f = stack.pop().unwrap();

                let fst = f.clone();
                let snd = Combinator::mult(Combinator::Y, f);

                stack.push(snd.into());
                stack.push(fst);
            }
            Multiple(a, b) => {
                stack.push(b);
                stack.push(a);
            }
            Other(val) => {
                if let Value::Function(fun) = val {
                    let res = fun.exec(&mut stack);
                    stack.push(res);
                } else {
                    unimplemented!("head: {:?}, stack: {:?}", val, stack);
                }
            }
        }

        stack
    }
}
