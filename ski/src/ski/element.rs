use super::Combinator;

#[derive(Clone, Debug, PartialEq)]
pub struct Element(pub Box<Combinator>);

impl std::fmt::Display for Element {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl<T: Into<Combinator>> From<T> for Element {
    fn from(a: T) -> Element {
        Element::new(a.into())
    }
}

impl Element {
    pub fn new(n: Combinator) -> Element {
        Element(Box::new(n))
    }
}

impl std::ops::Deref for Element {
    type Target = Combinator;

    fn deref(&self) -> &Combinator {
        &self.0
    }
}
