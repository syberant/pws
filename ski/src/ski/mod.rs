mod element;
mod combinator;
mod builtin;

pub use element::Element;
pub use combinator::Combinator;
pub use builtin::{Function, Value};

pub struct Program {
    instructions: Vec<Element>,
}

impl std::fmt::Display for Program {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        for i in self.instructions.iter().rev() {
            write!(f, "{}", i)?;
        }

        Ok(())
    }
}

impl Program {
    pub fn new(instructions: Vec<Element>) -> Program {
        Program { instructions }
    }

    pub fn execute(mut self) -> Element {
        while !self.is_done() {
            self.instructions = Combinator::step(self.instructions);
        }

        // println!("{}", self.instructions[0]);

        self.instructions.remove(0)
    }

    fn is_done(&self) -> bool {
        let one_elem = self.instructions.len() == 1;
        let is_singular = match *self.instructions[0].0 {
            Combinator::Multiple(..) => false,
            _ => true,
        };

        one_elem && is_singular
    }
}

#[test]
fn test_succ_zero() {
    let text = "add(1)(0)";
    let program = super::parse::parse_program(text).unwrap();
    println!("{}", program);

    let res = program.execute();

    assert_eq!(res, 1.into());

    // println!("{:?}", example);
}

#[test]
fn test_y_combinator() {
    let text = "
    (SI(K8))
(Y(
    S
    (S(KS)(
        S
        (S(KS)(S(S(KS)(S(S(KS)(S(KK)
            (Keq)
            ))(KI)))(S(KK)(K0))))
        (S(KK)(K6))
    ))
    (S(S(KS)(S(KK)I))(S(S(KS)(S(S(KS)(S(KK)(Ksub)))(KI)))(S(KK)(K1))))
))";
    let program = super::parse::parse_program(text).unwrap();
    let res = program.execute();

    assert_eq!(res, 6.into());
}
