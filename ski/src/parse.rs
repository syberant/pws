extern crate nom;

use crate::ski::{Combinator, Element, Function, Program, Value};
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::{char, digit1, multispace0};
use nom::combinator::{all_consuming, cut, map};
use nom::error::{context, convert_error, ParseError, VerboseError};
use nom::multi::fold_many1;
use nom::sequence::{delimited, preceded, terminated};
use nom::IResult;

pub fn unwrap_verbose_error(
    data: &str,
    error: Result<Program, nom::Err<VerboseError<&str>>>,
) -> Program {
    match error {
        Err(nom::Err::Error(e)) | Err(nom::Err::Failure(e)) => {
            let msg = convert_error(data, e);
            panic!("Error during parsing:\n{}", msg)
        }
        _ => error.unwrap(),
    }
}

pub fn parse_program(input: &str) -> Result<Program, nom::Err<(&str, nom::error::ErrorKind)>> {
    parse_program_generic(input)
}

pub fn parse_program_verbose(input: &str) -> Result<Program, nom::Err<VerboseError<&str>>> {
    parse_program_generic(input)
}

fn parse_program_generic<'a, E: ParseError<&'a str>>(
    input: &'a str,
) -> Result<Program, nom::Err<E>> {
    let (_, mut res) = all_consuming(fold_many1(parse_element, Vec::new(), |mut acc, new| {
        acc.push(new);
        acc
    }))(input)?;

    res.reverse();

    Ok(Program::new(res))
}

fn parse_element<'a, E: ParseError<&'a str>>(input: &'a str) -> IResult<&'a str, Element, E> {
    delimited(
        multispace0,
        alt((
            parse_char,
            parse_block,
            parse_builtin_func,
            parse_builtin_values,
        )),
        multispace0,
    )(input)
}

fn parse_builtin_func<'a, E: ParseError<&'a str>>(input: &'a str) -> IResult<&'a str, Element, E> {
    let add = map(tag("add"), |_| Function::Add.into());
    let sub = map(tag("sub"), |_| Function::Sub.into());
    let mul = map(tag("mul"), |_| Function::Mul.into());
    let div = map(tag("div"), |_| Function::Div.into());
    let modulus = map(tag("mod"), |_| Function::Mod.into());
    let eq  = map(tag("eq") , |_| Function::Eq.into());
    let lt = map(tag("lt"), |_| Function::Lt.into());
    let le = map(tag("le"), |_| Function::Le.into());
    let gt = map(tag("gt"), |_| Function::Gt.into());
    let ge = map(tag("ge"), |_| Function::Ge.into());

    alt((add, sub, mul, div, modulus, eq, lt, le, gt, ge))(input)
}

fn parse_builtin_values<'a, E: ParseError<&'a str>>(
    input: &'a str,
) -> IResult<&'a str, Element, E> {
    let number = map(parse_number, |n| Value::Number(n).into());

    // alt((number))(input)
    number(input)
}

fn parse_number<'a, E: ParseError<&'a str>, N: std::str::FromStr>(
    input: &'a str,
) -> IResult<&'a str, N, E>
where
    <N as std::str::FromStr>::Err: std::fmt::Debug,
{
    let is_number = digit1(input)?;
    // TODO: should I be error handling here?
    // Parsing the number should not be able to fail...
    let number = is_number.1.parse().unwrap();

    Ok((is_number.0, number))
}

fn parse_char<'a, E: ParseError<&'a str>>(input: &'a str) -> IResult<&'a str, Element, E> {
    let s = map(char('S'), |_| Combinator::S.into());
    let k = map(char('K'), |_| Combinator::K.into());
    let i = map(char('I'), |_| Combinator::I.into());
    let y = map(char('Y'), |_| Combinator::Y.into());

    alt((s, k, i, y))(input)
}

fn parse_block<'a, E: ParseError<&'a str>>(input: &'a str) -> IResult<&'a str, Element, E> {
    let b = char('(');
    let e = char(')');
    let f_acc = |acc, nval| {
        if let Some(aval) = acc {
            Some(add_to_existing_elem(aval, nval))
        } else {
            Some(nval)
        }
    };
    let m = map(fold_many1(parse_element, None, f_acc), |v| v.unwrap());

    context("block", preceded(b, cut(terminated(m, e))))(input)

    // The above version works much better with `VerboseError` than this one
    // delimited(b, m, e)(input)
}

fn add_to_existing_elem(old: Element, new: Element) -> Element {
    let c = match *old.0 {
        Combinator::Multiple(a, b) => {
            let left = Element::new(Combinator::Multiple(a, b));
            Combinator::Multiple(left, new)
        }
        n => Combinator::Multiple(Element::new(n), new),
    };

    Element::new(c)
}

#[test]
fn parse_ski() {
    // let text = "SII(SII)";
    let text = "S(S(Kadd)(K1))I3";
    let parsed = parse_program(text).unwrap();
    println!("{}", parsed);

    let res = parsed.execute();
    println!("{}", res);

    assert_eq!(res, 4.into());
}

#[test]
fn not() {
    // Source:
    //   let not = \b -> if b then False else True in if not False then 1 else 0
    let text = "S(S(S(SKK)(K(SK)))(K1))(K0)(S(S(SKK)(K(SK)))(KK))";
    let parsed = parse_program(text).unwrap();
    println!("{}", parsed);

    let res = parsed.execute();
    println!("{}", res);

    assert_eq!(res, 1.into());
}

#[test]
fn and() {
    // Source:
    //   let and = \a -> \b -> a && b
    //    in if and True False
    //      then 1
    //      else 0
    let text = "S(S(S(S(SKK)(KK))(K(SK)))(K1))(K0)(S(S(KS)(S(KK)(S(K(SK))(SKK))))(K(SKK)))";
    let parsed = parse_program(text).unwrap();
    println!("{}", parsed);

    let res = parsed.execute();
    println!("{}", res);

    assert_eq!(res, 0.into());
}

#[test]
fn or() {
    // Source:
    //   let or = \a -> \b -> a && b
    //    in if or True False
    //      then 1
    //      else 0
    let text = "S(S(S(S(SKK)(KK))(K(SK)))(K1))(K0)(S(S(KS)(S(KK)(S(KK)(SKK))))(K(SKK)))";
    let parsed = parse_program(text).unwrap();
    println!("{}", parsed);

    let res = parsed.execute();
    println!("{}", res);

    assert_eq!(res, 1.into());
}

#[test]
fn fac_5() {
    // Source:
    //   let rec fac = \n -> if n <= 1 then 1 else n * fac (n - 1)
    //    in fac 5
    let text = "S(SKK)(K5)(Y(S(K(S(S(S(S(Kle)(SKK))(K1))(K1))))(S(K(S(S(Kmul)(SKK))))(S(S(KS)(S(KK)(SKK)))(K(S(S(Ksub)(SKK))(K1)))))))";
    let parsed = parse_program(text).unwrap();
    println!("{}", parsed);

    let res = parsed.execute();
    println!("{}", res);

    assert_eq!(res, Value::Number(120).into());
}
