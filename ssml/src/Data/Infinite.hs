{-|
Module      : Data.Infinite
Description : Infinite sequences.
Copyright   : (C) Splinter Suidman, 2019
License     : AGPL-3
-}

module Data.Infinite
  ( Infinite (..)
  , infinite
  , infiniteWith
  , fromList
  , toList
  , head
  , tail
  , index
  , uncons
  , zip
  , zipWith
  , filter
  , take
  , intersperse
  , concat
  , concatMap
  ) where

import Prelude hiding (concat, concatMap, filter, head, tail, take, zip,
                zipWith)

-- | An infinite sequence of values of type @a@.
data Infinite a = a ::: Infinite a
  deriving (Eq, Show, Ord, Read)
infixr 5 :::

instance Functor Infinite where
  fmap f (x ::: xs) = f x ::: fmap f xs

instance Foldable Infinite where
  foldMap f (x ::: xs) = f x <> foldMap f xs

-- | 'infinite' @x@ repeats @x@ infinitely in the 'Infinite' data structure.
infinite :: a -> Infinite a
infinite x = x ::: infinite x

-- | 'infiniteWith' @x@ @f@ creates an 'Infinite' list, for which is true:
--
-- * For all indices @i >= 0@, @'index' ('infiniteWith' x f) i == applyN f i x@,
--   where @applyN f 0 = id@ and @applyN f n = f . applyN f (n - 1)@.
infiniteWith :: a -> (a -> a) -> Infinite a
infiniteWith x f = x ::: infiniteWith (f x) f

-- | 'fromList' @xs@ turns an infinite list @xs@ into the 'Infinite' data
-- structure. 'fromList' is a partial function, and does not work for finite
-- lists.
fromList :: [a] -> Infinite a
fromList = foldr (:::) (error "Data.Infinite.fromList: empty list")

-- | 'toList' @xs@ turns an 'Infinite' list @xs@ into a '[]' list.
toList :: Infinite a -> [a]
toList (x:::xs) = x : toList xs

-- | Extract the first element of an 'Infinite'.
head :: Infinite a -> a
head (x:::_) = x

-- | Extract the elements after the head of an 'Infinite'.
tail :: Infinite a -> Infinite a
tail (_:::xs) = xs

-- | 'index' @xs@ @n@ for @n >= 0@ returns the @n@th element of @xs@.
index :: Infinite a -> Int -> a
index (x:::_) 0  = x
index (_:::xs) n = index xs (n - 1)

-- | 'uncons' @(x:::xs)@ returns @(x, xs)@.
uncons :: Infinite a -> (a, Infinite a)
uncons (x:::xs) = (x, xs)

zip :: Infinite a -> Infinite b -> Infinite (a, b)
zip = zipWith (,)

zipWith :: (a -> b -> c) -> Infinite a -> Infinite b -> Infinite c
zipWith f (x:::xs) (y:::ys) = f x y ::: zipWith f xs ys

filter :: (a -> Bool) -> Infinite a -> Infinite a
filter p (x:::xs)
  | p x       = x ::: filter p xs
  | otherwise = filter p xs

take :: Int -> Infinite a -> [a]
take 0 _        = []
take n (x:::xs) = x : take (n - 1) xs

intersperse :: a -> Infinite a -> Infinite a
intersperse y (x:::xs) = x ::: y ::: intersperse y xs

concat :: Infinite [a] -> Infinite a
concat ([]:::ys)     = concat ys
concat ((x:xs):::ys) = x ::: concat (xs ::: ys)

concatMap :: (a -> [b]) -> Infinite a -> Infinite b
concatMap f = concat . fmap f
