module SSML.Typecheck
  ( TypecheckT (..)
  , runTypecheckT
  , Typecheck
  , runTypecheck
  , checkProgram
  , checkStmt
  , schemeOf
  , module Exports
  ) where

import SSML.Typecheck.Fresh as Exports
import SSML.Typecheck.Infer as Exports
import SSML.Typecheck.Unify as Exports

import Control.Monad.Except
import Control.Monad.Reader
import Data.Functor.Identity

import Data.Maybe

import SSML.Env    as Env
import SSML.Error
import SSML.Subst
import SSML.Syntax

newtype TypecheckT m a = TypecheckT { unTypecheckT :: ReaderT TypeEnv (ExceptT Error m) a }
  deriving (Functor, Applicative, Monad, MonadReader TypeEnv, MonadError Error)

runTypecheckT :: TypecheckT m a -> TypeEnv -> m (Either Error a)
runTypecheckT m env = runExceptT . flip runReaderT env . unTypecheckT $ m

type Typecheck a = TypecheckT Identity a

runTypecheck :: Typecheck a -> TypeEnv -> Either Error a
runTypecheck m env = runIdentity $ runTypecheckT m env

checkProgram :: (Monad m) => Program -> TypecheckT m ()
checkProgram (Program [])     = do
  env <- ask
  if isJust $ Env.lookup "main" env
    then pure ()
    else throwError NoMainFunction
checkProgram (Program (s:ss)) = checkStmt s (checkProgram (Program ss))

checkStmt :: (Monad m) => Stmt -> TypecheckT m a -> TypecheckT m a
checkStmt stmt@(SDecl x e) m = do
  -- TODO: waarschuwing voor variableschaduwen.
  s <- flip catchError (throwError . InStmt stmt) . schemeOf $ ELetRec x e (EVar x)
  local (insert (x, s)) m

schemeOf :: (Monad m) => Expr -> TypecheckT m Scheme
schemeOf expr = do
  env <- ask
  (ty, _fs, cs) <- either throwError pure $ runInfer (infer expr) env newInferState
  subst <- either throwError pure . runSolve $ solve cs
  let ty' = apply subst ty
  let s = simplScheme env . generalise env $ ty'
  pure s
