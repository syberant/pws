{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module SSML.Typecheck.Unify
  ( Constraint (..)
  , unify
  , solve
  , Solve
  , runSolve
  ) where

import Control.Monad.Except

import Text.Pretty

import SSML.Error
import SSML.Subst
import SSML.Syntax

infix 3 :~:
data Constraint
  = Type :~: Type
  -- | Een constructor die een expressie toevoegt, zodat betere foutmeldingen
  -- gegeven worden.
  | ConstraintExpr Constraint Expr
  deriving (Show, Eq)

instance Pretty Constraint where
  pretty (t1 :~: t2)          = pretty t1 <> " ~ " <> pretty t2
  pretty (ConstraintExpr c _) = pretty c

instance HasFreeVars Constraint where
  freeVars (t1 :~: t2)          = freeVars t1 <> freeVars t2
  freeVars (ConstraintExpr c _) = freeVars c

instance Substitutable Constraint Type where
  apply s (t1 :~: t2)             = apply s t1 :~: apply s t2
  apply s (ConstraintExpr c expr) = ConstraintExpr (apply s c) expr

type Solve a = Except Error a

runSolve :: Solve a -> Either Error a
runSolve = runExcept

solve :: (MonadError Error m) => [Constraint] -> m (Subst Type)
solve [] = pure $ Subst []
solve (c:cs) = do
  s1 <- unify c
  s2 <- solve $ fmap (apply s1) cs
  pure (apply s2 s1)

unify :: (MonadError Error m) => Constraint -> m (Subst Type)
unify = \case
  ConstraintExpr c expr ->
    flip catchError (throwError . InExpr expr) $ unify c

  TCon c1 :~: TCon c2
    | c1 == c2 -> pure (Subst [])

  t1     :~: TVar x -> bind x t1
  TVar x :~: t2     -> bind x t2

  t1 `TArr` t2 :~: t1' `TArr` t2' -> do
    s1 <- unify (t1 :~: t1')
    s2 <- unify (apply s1 t2 :~: apply s1 t2')
    pure (apply s2 s1)

  TProduct t1 t2 :~: TProduct t1' t2' -> solve [t1 :~: t1', t2 :~: t2']

  -- Otherwise
  t1 :~: t2 -> throwError $ TypesDoNotUnify t1 t2

bind :: (MonadError Error m) => Name -> Type -> m (Subst Type)
bind x t
  | t == TVar x    = pure $ Subst []
  | x `occursIn` t = throwError $ InfiniteType x t
  | otherwise      = pure $ Subst [(x, t)]
