module SSML.Compiler.SKI
  ( SKI (..)
  , skiT
  , skiF
  ) where

import qualified Data.Set    as Set
import           SSML.Subst
import           SSML.Syntax (EBinOp (..), Name (..))
import           Text.Pretty

data SKI
  = S
  | K
  --- | I
  | Y
  | SKIInt Integer
  | SKIVar Name
  | SKIApp SKI SKI
  | SKIAbs Name SKI
  | SKIBinOp EBinOp
  deriving (Show, Eq)

instance HasFreeVars SKI where
  freeVars = \case
    S -> []
    K -> []
    -- I -> []
    Y -> []
    SKIInt {} -> []
    SKIVar x -> [x]
    SKIAbs x e1 -> freeVars e1 `Set.difference` [x]
    SKIApp e1 e2 -> freeVars e1 <> freeVars e2
    SKIBinOp {} -> []

instance Pretty SKI where
  pretty e = case e of
    S -> "S"
    K -> "K"
    -- I -> "i"
    Y -> "Y"
    SKIVar x -> pretty x
    SKIAbs x e1 -> "\\" <> pretty x <> ". " <> pretty e1
    SKIApp e1 e2 ->
      let e1' = pretty e1
          e2' = if isSimple e2 then pretty e2 else "(" <> pretty e2 <> ")"
       in e1' <> e2'
    SKIInt i -> pretty i
    SKIBinOp b -> case b of
      EAdd -> "add"
      ESub -> "sub"
      EMul -> "mul"
      EDiv -> "div"
      EMod -> "mod"
      EAnd -> pretty skiF
      EOr  -> pretty skiT
      ELT  -> "lt"
      ELE  -> "le"
      EGT  -> "gt"
      EGE  -> "ge"
    where
      isSimple = \case
        SKIApp {} -> False
        SKIAbs {} -> False
        _         -> True

skiT :: SKI
skiT = K

skiF :: SKI
skiF = S `SKIApp` K
