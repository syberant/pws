{-# LANGUAGE FlexibleInstances #-}

module SSML.Env
  ( Env (..)
  , emptyEnv
  , lookup
  , insert
  ) where

import Prelude hiding (insert, lookup)

import           Data.List       (intercalate)
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map

import Text.Pretty

import SSML.Subst
import SSML.Syntax

newtype Env v = Env { unEnv :: Map Name v }
  deriving
    ( Show
    , Eq
    , Semigroup
    , Monoid
    , Functor
    , Foldable -- Gives free instance of 'HasFreeVars v => HasFreeVars (Env v)'.
    )

emptyEnv :: Env v
emptyEnv = Env []

instance (Pretty v) => Pretty (Env v) where
  pretty (Env []) = "{}"
  pretty (Env e) = "{ " <> bindings <> "\n}"
    where
      bindings = intercalate "\n, " . fmap (\(x, s) -> pretty x <> " : " <> pretty s) $ Map.toList e

instance Substitutable v s => Substitutable (Env v) s where
  apply s = Env . apply s . unEnv

insert :: (Name, v) -> Env v -> Env v
insert (n, v) = mappend (Env [(n, v)])

lookup :: Name -> Env v -> Maybe v
lookup n = Map.lookup n . unEnv
