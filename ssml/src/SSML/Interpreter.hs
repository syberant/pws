{-# LANGUAGE FlexibleContexts #-}

module SSML.Interpreter
  ( InterpreterT (..)
  , runInterpreterT
  , Interpreter
  , runInterpreter
  , ValueEnv
  , eval
  , evalStmt
  , evalExpr
  , inEnv
  ) where

import Control.Monad.Reader
import Data.Functor.Identity

import Text.Pretty

import SSML.Env    as Env
import SSML.Subst
import SSML.Syntax

newtype InterpreterT m a = InterpreterT { unInterpreterT :: ReaderT ValueEnv m a }
  deriving (Functor, Applicative, Monad, MonadReader ValueEnv)

runInterpreterT :: InterpreterT m a -> ValueEnv -> m a
runInterpreterT m e = flip runReaderT e . unInterpreterT $ m

type Interpreter a = InterpreterT Identity a

runInterpreter :: Interpreter a -> ValueEnv -> a
runInterpreter m e = runIdentity $ runInterpreterT m e

type ValueEnv = Env Expr

inEnv :: (MonadReader ValueEnv m) => (Name, Expr) -> m a -> m a
inEnv binding = local (insert binding)

lookupEnv :: Name -> Interpreter Expr
lookupEnv x = do
  env <- ask
  case Env.lookup x env of
    Nothing -> error "Als deze fout voorkomt, zit er waarschijnlijk een fout in de typechecker."
    Just e -> pure e

eval :: Program -> Interpreter ()
eval (Program []) = pure ()
eval (Program (s:ss)) =
  evalStmt s $ eval (Program ss)

evalStmt :: Stmt -> Interpreter a -> Interpreter a
evalStmt (SDecl x e) m = do
  v <- evalExpr e
  inEnv (x, v) m

evalExpr :: Expr -> Interpreter Expr
evalExpr expr =
  case expr of
    EVar x -> lookupEnv x
    ELit l -> pure (ELit l)
    EApp e1 e2 -> do
      v1 <- evalExpr e1
      v2 <- evalExpr e2
      case v1 of
        EAbs x e1' ->
          let e = apply (Subst [(x, v2)]) e1'
           in evalExpr e
        _ -> blameTypechecker expr
    EAbs x e1 -> pure (EAbs x e1)
    ELet x e1 e2 -> do
      v1 <- evalExpr e1
      inEnv (x, v1) $ evalExpr e2
    ELetRec x e1 e2 -> evalExpr $ ELet x (EFix (EAbs x e1)) e2
    EIf e1 e2 e3 -> do
      v1 <- evalExpr e1
      let pred =
            case v1 of
              ELit (EBool b) -> b
              _              -> blameTypechecker expr
      if pred
        then evalExpr e2
        else evalExpr e3
    EProduct e1 e2 -> EProduct <$> evalExpr e1 <*> evalExpr e2
    EFix e1 -> do
      v1 <- evalExpr e1
      case v1 of
        EAbs x e2 ->
          let e2' = apply (Subst [(x, expr)]) e2
           in evalExpr e2'
        _ -> blameTypechecker expr
    EBinOp op e1 e2 ->
      case op of
        EAdd -> evalBinOpInt (\a b -> ELit . EInt $ a + b) e1 e2
        ESub -> evalBinOpInt (\a b -> ELit . EInt $ a - b) e1 e2
        EMul -> evalBinOpInt (\a b -> ELit . EInt $ a * b) e1 e2
        EDiv -> evalBinOpInt (\a b -> ELit . EInt $ a `div` b) e1 e2
        EMod -> evalBinOpInt (\a b -> ELit . EInt $ a `mod` b) e1 e2
        EAnd -> evalBinOpBool (\a b -> ELit . EBool $ a && b) e1 e2
        EOr  -> evalBinOpBool (\a b -> ELit . EBool $ a || b) e1 e2
        ELT  -> evalBinOpInt (\a b -> ELit . EBool $ a < b) e1 e2
        ELE  -> evalBinOpInt (\a b -> ELit . EBool $ a <= b) e1 e2
        EGT  -> evalBinOpInt (\a b -> ELit . EBool $ a > b) e1 e2
        EGE  -> evalBinOpInt (\a b -> ELit . EBool $ a >= b) e1 e2
  where
    evalBinOpInt op e1 e2 = do
      v1 <- evalExpr e1
      v2 <- evalExpr e2
      case (v1, v2) of
        (ELit (EInt a), ELit (EInt b)) -> pure $ a `op` b
        _                              -> blameTypechecker expr
    evalBinOpBool op e1 e2 = do
      v1 <- evalExpr e1
      v2 <- evalExpr e2
      case (v1, v2) of
        (ELit (EBool a), ELit (EBool b)) -> pure $ a `op` b
        _                                -> blameTypechecker expr


blameTypechecker :: Expr -> a
blameTypechecker expr = error $
  "Als deze fout voorkomt, zit er waarschijnlijk een fout in de typechecker.\n" <>
  "expr = " <> show expr <> "\n" <>
  "pretty expr = " <> pretty expr
