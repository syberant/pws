module SSML.Compiler
  ( module Exports
  , CompilerT (..)
  , runCompilerT
  , Compiler (..)
  , runCompiler
  , compileProgram
  , compileStmt
  , compileExpr
  , Abstractable (..)
  ) where

import SSML.Compiler.SKI as Exports

import Control.Monad.Except
import Control.Monad.Reader
import Data.Functor.Identity

import SSML.Env    as Env
import SSML.Error
import SSML.Subst
import SSML.Syntax

type SKIEnv = Env SKI

newtype CompilerT m a = CompilerT { unCompilerT :: ReaderT SKIEnv (ExceptT Error m) a }
  deriving (Functor, Applicative, Monad, MonadReader SKIEnv, MonadError Error)

runCompilerT :: CompilerT m a -> SKIEnv -> m (Either Error a)
runCompilerT m env = runExceptT . flip runReaderT env . unCompilerT $ m

type Compiler a = CompilerT Identity a

runCompiler :: Compiler a -> SKIEnv -> Either Error a
runCompiler m env = runIdentity $ runCompilerT m env

compileProgram :: (Monad m) => Program -> CompilerT m SKI
-- Als de typechecker is geslaagd, zou er een main-functie moeten bestaan.
compileProgram (Program [])     = compileExpr "main"
compileProgram (Program (s:ss)) = compileStmt s (compileProgram (Program ss))

compileStmt :: (Monad m) => Stmt -> CompilerT m a -> CompilerT m a
compileStmt stmt@(SDecl x e) m = do
  e' <- compileExpr $ ELetRec x e (EVar x)
  local (insert (x, e')) m

compileExpr :: (Monad m) => Expr -> CompilerT m SKI
compileExpr = abstract
-- TODO: abstract kan ook een fout maken, als niet alle variabelen worden
-- verwijderd. Moet dit gecheckt worden, of gaan we ervan uit dat dit niet
-- gebeurt?

class Abstractable a where
  abstract :: (Monad m) => a -> CompilerT m SKI

instance Abstractable Expr where
  abstract expr = flip catchError (throwError . InExpr expr) $ do
    case expr of
      EVar x -> do
        env <- ask
        case Env.lookup x env of
          Just ski -> pure ski
          Nothing  -> pure (SKIVar x)
      ELit (EInt i) -> pure (SKIInt i)
      ELit (EBool b) -> pure $ if b then skiT else skiF
      EApp e1 e2 -> SKIApp <$> abstract e1 <*> abstract e2
      ELet x e1 e2 -> abstract ((EAbs x e2) `EApp` e1)
      ELetRec x e1 e2 -> abstract (ELet x (EFix (EAbs x e1)) e2)
      EIf e1 e2 e3 -> (\e1' e2' e3' -> e1' `SKIApp` e2' `SKIApp` e3')
        <$> abstract e1
        <*> abstract e2
        <*> abstract e3
      EProduct {} -> undefined
      EFix e1 -> (Y `SKIApp`) <$> abstract e1
      EBinOp b e1 e2 -> (\b' e1' e2' -> b' `SKIApp` e1' `SKIApp` e2')
        <$> abstract (SKIBinOp b)
        <*> abstract e1
        <*> abstract e2
      EAbs x e1 -> do
        e1' <- abstract e1
        case e1' of
          SKIVar y | x == y -> pure $ S `SKIApp` K `SKIApp` K
          _ | not (x `occursIn` e1') -> pure $ K `SKIApp` e1'
          SKIApp m n -> (\m' n' -> S `SKIApp` m' `SKIApp` n')
            <$> abstract (SKIAbs x m)
            <*> abstract (SKIAbs x n)
          SKIAbs {} -> error "Dit zou onmogelijk moeten zijn, gezien abstract SKIAbs weghaalt."

instance Abstractable SKI where
  abstract = \case
    S -> pure S
    K -> pure K
    -- I -> I
    Y -> pure Y
    SKIVar x -> do
      env <- ask
      case Env.lookup x env of
        Just ski -> pure ski
        Nothing  -> pure (SKIVar x)
    SKIInt i -> pure (SKIInt i)
    SKIApp e1 e2 -> SKIApp <$> abstract e1 <*> abstract e2
    SKIAbs x e1 -> do
      e1' <- abstract e1
      case e1' of
        SKIVar y | x == y -> pure $ S `SKIApp` K `SKIApp` K
        _ | not (x `occursIn` e1') -> pure $ K `SKIApp` e1'
        SKIApp m n -> (\m' n' -> S `SKIApp` m' `SKIApp` n')
          <$> abstract (SKIAbs x m)
          <*> abstract (SKIAbs x n)
        SKIAbs {} -> error "Dit zou onmogelijk moeten zijn, gezien abstract SKIAbs weghaalt."
    SKIBinOp EAnd -> pure skiF
    SKIBinOp EOr -> pure skiT
    SKIBinOp b -> pure (SKIBinOp b)
