module SSML.Error
  ( Error (..)
  ) where

import Data.List (intercalate)

import Text.Pretty

import SSML.Env    (Env)
import SSML.Syntax

instance Semigroup Error where
  (<>) = And

data Error
  = UnboundVar Name
  | TypesDoNotUnify Type Type
  | InfiniteType Name Type
  | WithEnv (Env Scheme) Error
  | InExpr Expr Error
  | InStmt Stmt Error
  | NoMainFunction
  | And Error Error
  deriving (Show, Eq)

instance Pretty Error where
  pretty (UnboundVar x) =
    "Unbound variable: ‘" <> pretty x <> "’"
  pretty (TypesDoNotUnify t1 t2) =
    "Cannot unify constraint: " <> pretty t1 <> " ~ " <> pretty t2
  pretty (InfiniteType x t) =
    "Occurs check: cannot construct infinite type: " <> pretty x <> " ~ " <> pretty t
  pretty (WithEnv env e) =
    pretty e <> "\n  In environment:\n    " <> (intercalate "\n    " . lines . pretty $ env)
  pretty (InExpr expr e) =
    pretty e <> "\n  In expression: " <> pretty expr
  pretty (InStmt stmt e) =
    pretty e <> "\n  In statement: " <> pretty stmt
  pretty NoMainFunction =
    "No function with name ‘main’ found, needed for compiling the program."
  pretty (And e1 e2) =
    pretty e1 <> "\n" <> pretty e2
