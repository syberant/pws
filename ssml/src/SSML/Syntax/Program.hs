module SSML.Syntax.Program
  ( Program (..)
  ) where

import Text.Pretty

import SSML.Syntax.Stmt

newtype Program = Program { unProgram :: [Stmt] }
  deriving (Show, Eq, Semigroup, Monoid)

instance Pretty Program where
  pretty = unlines . fmap pretty . unProgram
