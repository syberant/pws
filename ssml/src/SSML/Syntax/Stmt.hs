module SSML.Syntax.Stmt
  ( Stmt (..)
  ) where

import Text.Pretty

import SSML.Syntax.Expr
import SSML.Syntax.Name

data Stmt
  = SDecl Name Expr
  deriving (Show, Eq)

instance Pretty Stmt where
  pretty = \case
    SDecl x e -> pretty x <> " = " <> pretty e
