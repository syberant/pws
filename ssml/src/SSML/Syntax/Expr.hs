module SSML.Syntax.Expr
  ( Expr (..)
  , ELit (..)
  , eBool
  , eInt
  , EBinOp (..)
  , eAdd
  , eSub
  , eMul
  , eDiv
  , eMod
  , eAnd
  , eOr
  , eLT
  , eLE
  , eGT
  , eGE
  ) where

import           Data.List       (intercalate)
import qualified Data.Map.Strict as Map
import qualified Data.Set        as Set
import           Data.String

import Text.Pretty

import SSML.Subst
import SSML.Syntax.Name
import SSML.Syntax.Support

data Expr
  -- | Variabele.
  = EVar Name
  -- | ‘Literal’-expressie.
  | ELit ELit
  -- | Functieaanroep.
  | EApp Expr Expr
  -- | Lambda-abstractie.
  | EAbs Name Expr
  -- | @let@-@in@-expressie.
  | ELet Name Expr Expr
  -- | @letrec@-@in@-expressie, een recursieve @let@-@in@-expressie.
  | ELetRec Name Expr Expr
  -- | @if@-@then@-@else@-expressie.
  | EIf Expr Expr Expr
  -- | Paarexpressie.
  | EProduct Expr Expr
  --- -- | TODO: Somexpressie.
  --- | ESum (Either Expr Expr)
  -- | Dekpuntexpressie.
  | EFix Expr
  -- | Binaire bewerkingen.
  | EBinOp EBinOp Expr Expr
  deriving (Show, Eq, Ord)

instance Pretty Expr where
  pretty e = case e of
    EVar n -> pretty n
    ELit l -> pretty l
    EApp e1 e2 ->
      let e1' = applyIf (precedence e1 <  p) parenthesise . pretty $ e1
          e2' = applyIf (precedence e2 <= p) parenthesise . pretty $ e2
       in e1' <> " " <> e2'
    EAbs x e1 -> "\\" <> pretty x <> " -> " <> pretty e1
    ELet x e1 e2 -> "let " <> pretty x <> " = " <> pretty e1 <> " in " <> pretty e2
    ELetRec x e1 e2 -> "let rec " <> pretty x <> " = " <> pretty e1 <> " in " <> pretty e2
    EIf e1 e2 e3 -> "if " <> pretty e1 <> " then " <> pretty e2 <> " else " <> pretty e3
    EProduct e1 e2 -> "(" <> pretty e1 <> ", " <> pretty e2 <> ")"
    EFix e1 ->
      let e1' =
            applyIf
              (precedence e1 <= precedence (EApp undefined undefined))
              parenthesise
              (pretty e1)
       in "fix " <> e1'
    EBinOp op e1 e2 ->
      let (l, r) = precedenceFixity (fixity op)
          e1' =
            applyIf
              (precedence e1 `l` p) parenthesise . pretty $ e1
          e2' =
            applyIf
              (precedence e2 `r` p) parenthesise . pretty $ e2
       in e1' <> " " <> pretty op <> " " <> e2'
    where
      precedenceFixity :: (Ord a) => Fixity -> (a -> a -> Bool, a -> a -> Bool)
      precedenceFixity = \case
        Infix -> ((<=), (<=))
        InfixL -> ((<), (<=))
        InfixR -> ((<=), (<))
      p = precedence e
      precedence :: Expr -> Int
      precedence = \case
        EVar {} -> 7
        ELit {} -> 7
        EProduct {} -> 7
        EApp {} -> 6
        EBinOp op _ _ -> case op of
          EAdd {} -> 4
          ESub {} -> 4
          EMul {} -> 5
          EDiv {} -> 5
          EMod {} -> 5
          EAnd {} -> 2
          EOr {}  -> 1
          ELT {}  -> 3
          ELE {}  -> 3
          EGT {}  -> 3
          EGE {}  -> 3
        EFix {} -> 0
        EAbs {} -> 0
        ELet {} -> 0
        ELetRec {} -> 0
        EIf {} -> 0

instance IsString Expr where
  fromString = EVar . fromString

instance HasFreeVars Expr where
  freeVars e =
    case e of
      EVar x          -> [x]
      ELit {}         -> []
      EApp e1 e2      -> freeVars e1 <> freeVars e2
      EAbs x e1       -> freeVars e1 `Set.difference` [x]
      ELet x e1 e2    -> freeVars e1 <> (freeVars e2 `Set.difference` [x])
      ELetRec x e1 e2 -> (freeVars e1 <> freeVars e2) `Set.difference` [x]
      EIf e1 e2 e3    -> freeVars e1 <> freeVars e2 <> freeVars e3
      EProduct e1 e2  -> freeVars e1 <> freeVars e2
      EFix e1         -> freeVars e1
      EBinOp op e1 e2 -> freeVars e1 <> freeVars e2

instance Substitutable Expr Expr where
  apply s@(Subst subst) e = case e of
    EVar x -> Map.findWithDefault e x subst
    ELit l -> ELit l
    EApp e1 e2 -> EApp (apply s e1) (apply s e2)
    EAbs x e1 -> EAbs x $ apply (Subst $ Map.delete x subst) e1
    ELet x e1 e2 ->
      -- Only apply s' to e2.
      let s' = Subst $ Map.delete x subst
       in ELet x (apply s e1) (apply s' e2)
    ELetRec x e1 e2 ->
      -- Apply s' to both e1 and e2.
      let s' = Subst $ Map.delete x subst
       in ELetRec x (apply s' e1) (apply s' e2)
    EIf e1 e2 e3 -> EIf (apply s e1) (apply s e2) (apply s e3)
    EProduct e1 e2 -> EProduct (apply s e1) (apply s e2)
    EFix e1 -> EFix (apply s e1)
    EBinOp op e1 e2 -> EBinOp op (apply s e1) (apply s e2)

data ELit
  -- | Booleanexpressie.
  = EBool Bool
  -- | Getalexpressie.
  | EInt Integer
  deriving (Show, Eq, Ord)

eBool :: Bool -> Expr
eBool = ELit . EBool

eInt :: Integer -> Expr
eInt = ELit . EInt

instance Pretty ELit where
  pretty = \case
    EBool b -> pretty b
    EInt i  -> pretty i

-- TODO: documenteren.
data EBinOp
  = EAdd
  | ESub
  | EMul
  | EDiv
  | EMod
  | EAnd
  | EOr
  | ELT
  | ELE
  | EGT
  | EGE
  deriving (Show, Eq, Ord)

eAdd, eSub, eMul, eDiv, eMod, eAnd, eOr, eLT, eLE, eGT, eGE :: Expr -> Expr -> Expr
eAdd = EBinOp EAdd
eSub = EBinOp ESub
eMul = EBinOp EMul
eDiv = EBinOp EDiv
eMod = EBinOp EMod
eAnd = EBinOp EAnd
eOr = EBinOp EOr
eLT = EBinOp ELT
eLE = EBinOp ELE
eGT = EBinOp EGT
eGE = EBinOp EGE

instance Pretty EBinOp where
  pretty = \case
    EAdd -> "+"
    ESub -> "-"
    EMul -> "*"
    EDiv -> "/"
    EMod -> "%"
    EAnd -> "&&"
    EOr  -> "||"
    ELT  -> "<"
    ELE  -> "<="
    EGT  -> ">"
    EGE  -> ">="

data Fixity
  = InfixL
  | Infix
  | InfixR
  deriving (Show, Eq, Ord)

fixity :: EBinOp -> Fixity
fixity = \case
  EAdd -> InfixL
  ESub -> InfixL
  EMul -> InfixL
  EDiv -> InfixL
  EMod -> InfixL
  EAnd -> InfixR
  EOr  -> InfixR
  ELT  -> Infix
  ELE  -> Infix
  EGT  -> Infix
  EGE  -> Infix
