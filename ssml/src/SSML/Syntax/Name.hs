module SSML.Syntax.Name
  ( Name (..)
  ) where

import Data.String
import Data.Text
import Text.Pretty

newtype Name = Name { getName :: Text }
  deriving (Show, Eq, Ord, Semigroup, Monoid, Read)

instance Pretty Name where
  pretty = unpack . getName

instance IsString Name where
  fromString = Name . fromString
