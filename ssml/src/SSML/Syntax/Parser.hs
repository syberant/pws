module SSML.Syntax.Parser
  ( Result (..)
  , getResult
  , Parser
  , parseExpr
  , parseStmt
  , parseType
  , parseScheme
  , parseProgram
  , complete
  ) where

import           Comb                  hiding (Parser)
import qualified Comb
import           Data.Char
import           Data.Foldable         (asum)
import           Data.Functor.Identity
import           Data.List.NonEmpty
import           Data.String
import           Data.Text             (Text)
import qualified Data.Text             as Text

import SSML.Syntax.Expr
import SSML.Syntax.Name
import SSML.Syntax.Program
import SSML.Syntax.Scheme
import SSML.Syntax.Stmt
import SSML.Syntax.Type

type Parser a = Comb.Parser (Result [String]) (Positioned Text) a

getResult :: Result e a -> Either e a
getResult (Ok (a:|_)) = Right a
getResult (Err e)     = Left e

data Result e a
  = Ok (NonEmpty a)
  | Err e
  deriving (Show, Eq)

instance Functor (Result e) where
  fmap f (Ok as) = Ok (fmap f as)
  fmap _ (Err e) = Err e

instance (Monoid e) => Applicative (Result e) where
  pure = Ok . pure
  (Ok f)  <*> (Ok a)  = Ok (f <*> a)
  (Err e) <*> (Ok _)  = Err e
  (Ok _)  <*> (Err e) = Err e
  (Err e) <*> (Err f) = Err (e <> f)

instance (Monoid e) => Alternative (Result e) where
  empty = Err mempty
  (Ok a)  <|> (Ok b)  = Ok (a <> b)
  (Ok a)  <|> (Err _) = Ok a
  (Err _) <|> (Ok b)  = Ok b
  (Err e) <|> (Err f) = Err (e <> f)

instance (Monoid e) => GreedyAlternative (Result e) where
  (Ok a)  <||> (Ok b)  = Ok (a <||> b)
  (Ok a)  <||> (Err _) = Ok a
  (Err _) <||> (Ok b)  = Ok b
  (Err e) <||> (Err f) = Err (e <> f)

instance (Monoid e) => Monad (Result e) where
  (Ok a) >>= f = asum (f <$> a)
  (Err e) >>= _ = Err e

instance (Monoid e) => ParserMonad (Result e)

-- ** Helper parsers.

complete :: Parser a -> Parser a
complete = (<* eof)

whitespace :: Parser ()
whitespace = () <$ greedy (satisfy isSpace)

whitespace1 :: Parser ()
whitespace1 = () <$ greedy1 (satisfy isSpace)

lowerName :: Parser Name
lowerName = fmap fromString $
  (:) <$> begin <*> greedy rest
  where
    begin = lower <|> symbol '_'
    rest = alphaNum <|> symbol '_'

upperName :: Parser Name
upperName = fmap fromString $
  (:) <$> begin <*> greedy rest
  where
    begin = upper
    rest = alphaNum <|> symbol '_'

lambda :: Parser Char
lambda = symbol '\\' <|> symbol 'λ'

arrow :: Parser String
arrow = token "->" <|> token "→"

integer :: Parser Integer
integer = foldl (\a b -> a * 10 + fromIntegral (ord b - ord '0')) 0 <$> many1 digit

-- | 'tuple' parses a tuple with /two/ or more elements.
tuple :: Parser a -> Parser [a]
tuple p =
  (\_ a _ as _ -> a : as)
  <$> (symbol '(' *> whitespace)
  <*> p
  <*> (whitespace *> symbol ',' *> whitespace)
  <*> p `sepBy` (whitespace *> symbol ',' *> whitespace)
  <*> (whitespace *> symbol ')')

-- ** Parsers for 'Type'.

parseType :: Parser Type
parseType = whitespace *> type1 <* whitespace

type1 :: Parser Type
type1 = chainr type2 (TArr <$ (whitespace *> arrow *> whitespace))

type2 :: Parser Type
type2 = chainl type3 (TProduct <$ (whitespace *> symbol '*' *> whitespace))

type3 :: Parser Type
type3
  =  TCon <$> upperName
 <|> TVar <$> lowerName
 <|> parenthesised parseType

-- ** Parsers for 'Scheme'.

parseScheme :: Parser Scheme
parseScheme = whitespace *> scheme1 <* whitespace

scheme1 :: Parser Scheme
scheme1
  =  (\_ as _ t -> Forall as t)
     <$> ((symbol '∀' *> whitespace) <|> (token "forall" *> whitespace1))
     <*> lowerName `sepBy` whitespace1
     <*> (whitespace *> symbol '.' *> whitespace)
     <*> parseType
 <|> Forall [] <$> parseType

-- ** Parsers for 'Expr'.

parseExpr :: Parser Expr
parseExpr = whitespace *> expr1 <* whitespace

expr1 :: Parser Expr
expr1
  =  (\_ x _ e1 -> EAbs x e1)
     <$> (lambda *> whitespace)
     <*> lowerName
     <*> (whitespace *> arrow *> whitespace)
     <*> expr1
 <|> (\_ f _ e2 -> f e2)
     <$> (token "let" *> whitespace1)
     <*> chainr
         ((\x _ e1 -> ELet x e1)
          <$> lowerName
          <*> (whitespace *> symbol '=' *> whitespace)
          <*> expr1
         )
         ((.) <$ (whitespace *> symbol ';' *> whitespace))
     <*> (whitespace1 *> token "in" *> whitespace1)
     <*> expr1
 <|> (\_ x _ e1 _ e2 -> ELetRec x e1 e2)
     <$> (token "let" *> whitespace1 *> token "rec" *> whitespace1)
     <*> lowerName
     <*> (whitespace *> symbol '=' *> whitespace)
     <*> expr1
     <*> (whitespace1 *> token "in" *> whitespace1)
     <*> expr1
 <|> (\_ e1 _ e2 _ e3 -> EIf e1 e2 e3)
     <$> (token "if" *> whitespace1)
     <*> expr1
     <*> (whitespace1 *> token "then" *> whitespace1)
     <*> expr1
     <*> (whitespace1 *> token "else" *> whitespace1)
     <*> expr1
 <|> expr2

expr2 :: Parser Expr
expr2 = chainr expr3 (EBinOp EOr <$ (whitespace *> token "||" *> whitespace))

expr3 :: Parser Expr
expr3 = chainr expr4 (EBinOp EAnd <$ (whitespace *> token "&&" *> whitespace))

expr4 :: Parser Expr
expr4
  =  (\e1 _ op _ e2 -> EBinOp op e1 e2)
     <$> expr5
     <*> whitespace
     <*> (  ELT <$ token "<"
        <|> ELE <$ token "<="
        <|> EGT <$ token ">"
        <|> EGE <$ token ">="
         )
     <*> whitespace
     <*> expr5
 <|> expr5

expr5 :: Parser Expr
expr5 = chainl expr6 (whitespace *> op <* whitespace)
  where
    op =  EBinOp EAdd <$ token "+"
      <|> EBinOp ESub <$ token "-"

expr6 :: Parser Expr
expr6 = chainl expr7 (whitespace *> op <* whitespace)
  where
    op =  EBinOp EMul <$ token "*"
      <|> EBinOp EDiv <$ token "/"
      <|> EBinOp EMod <$ token "%"

expr7 :: Parser Expr
expr7
  =  token "fix" *> whitespace1 *> (EFix <$> expr8)
 <|> chainl expr8 (EApp <$ whitespace)

expr8 :: Parser Expr
expr8
  =  exprLit
 <|> EVar <$> lowerName
 <|> parenthesised parseExpr
 <|> parenthesised (chainl parseExpr (EProduct <$ (whitespace *> symbol ',' *> whitespace)))

exprLit :: Parser Expr
exprLit
  =  ELit (EBool True) <$ token "True"
 <|> ELit (EBool False) <$ token "False"
 <|> ELit . EInt <$> integer

-- ** Parsers for 'Stmt'.

parseStmt :: Parser Stmt
parseStmt = whitespace *> stmt1 <* whitespace

stmt1 :: Parser Stmt
stmt1
  =  (\x _ e -> SDecl x e)
     <$> lowerName
     <*> (whitespace *> symbol '=' *> whitespace)
     <*> parseExpr

-- ** Parsers for 'Program'.

parseProgram :: Parser Program
parseProgram = whitespace *> program1 <* whitespace

program1 :: Parser Program
program1 =
  Program <$>
    (parseStmt `sepBy` (whitespace *> symbol ';' *> whitespace))
