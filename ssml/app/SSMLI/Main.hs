module Main where

import Prelude hiding (print, read)

import Comb                     hiding (print)
import Control.Lens
import Control.Monad.Reader
import Data.String
import System.Console.Haskeline hiding (complete)
import Text.Pretty

import SSML.Env
import SSML.Interpreter hiding (eval)
import SSML.Syntax
import SSML.Typecheck

type Repl a = ReaderT ReplEnv (InputT IO) a

data ReplEnv = ReplEnv
  { _typeEnv  :: TypeEnv
  , _valueEnv :: ValueEnv
  } deriving (Show, Eq)
makeLenses ''ReplEnv

main :: IO ()
main = runRepl repl newReplEnv

runRepl :: Repl a -> ReplEnv -> IO a
runRepl m e
  = runInputT defaultSettings
  . flip runReaderT e
  $ m

newReplEnv :: ReplEnv
newReplEnv = ReplEnv
  { _typeEnv = Env []
  , _valueEnv = Env []
  }

repl :: Repl ()
repl = do
  minput <- lift $ getInputLine "λ> "
  case minput of
    Nothing -> pure ()
    Just ":q" -> pure ()
    Just input -> do
      stmt <- read input
      case stmt of
        Nothing -> do
          lift . outputStrLn $ "Parse error."
          repl
        Just s -> eval s repl

read :: String -> Repl (Maybe Stmt)
read input = do
  let res = getResult $ runParser
        (complete parseStmt)
        (Positioned
          (Position { line = 0, column = 0, file = Just "REPL" })
          (fromString input))
  case res of
    Left _       -> pure Nothing
    Right (s, _) -> pure (Just s)

typecheck :: Expr -> Repl (Maybe Scheme)
typecheck expr = do
  env <- asks $ view typeEnv
  let res = runTypecheck (schemeOf expr) env
  case res of
    Left err -> do
      lift . outputStrLn . pretty $ err
      pure Nothing
    Right s  -> pure (Just s)

eval :: Stmt -> Repl a -> Repl a
eval (SDecl x e) m = do
  let e' = EFix (EAbs x e)
  ms <- typecheck e'
  case ms of
    Nothing -> m
    Just s -> do
      vEnv <- asks $ view valueEnv
      let v = runInterpreter (evalExpr e') vEnv
      print s (EVar x)
      local (over typeEnv (insert (x, s)) . over valueEnv (insert (x, v))) m
-- TODO: aangepaste Stmt voor de Repl, die SExpr bevat.

print :: Scheme -> Expr -> Repl ()
print s e = lift $ outputStrLn $ pretty e <> "\n  : " <> pretty s
