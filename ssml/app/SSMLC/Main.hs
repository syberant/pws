module Main where

import Comb
import Data.String
import SSML
import System.Environment
import Text.Pretty

main :: IO ()
main = do
  args <- getArgs
  filename <- case args of
    filename:_ -> pure filename
    _          -> ioError . userError $ "Usage: ssmlc [input file]."

  putStrLn $ "ssmlc: reading input file ‘" <> filename <> "’."
  file <- readFile filename

  putStrLn "ssmlc: parsing input file."
  let res = getResult $ runParser
        (parseProgram)
        (Positioned
          (Position { line = 0, column = 0, file = Just filename })
          (fromString file))
  program <- case res of
    Left _       -> ioError . userError $ "Parse error."
    Right (p, _) -> pure p

  putStrLn "ssmlc: typechecking program."
  typecheck program

  putStrLn "ssmlc: compiling program."
  ski <- compile program

  putStrLn $ "ssmlc: writing to file ‘" <> filename <> ".ski’."
  writeFile (filename <> ".ski") (pretty ski)

  putStrLn "ssmlc: succes!"

typecheck :: Program -> IO ()
typecheck program = do
  let res = runTypecheck (checkProgram program) (Env [])
  case res of
    Left err -> do
      ioError . userError $ "Error while typechecking program:\n" <> pretty err
    Right () -> pure ()

compile :: Program -> IO SKI
compile program = do
  let res = runCompiler (compileProgram program) (Env [])
  case res of
    Left err -> do
      ioError . userError $ "Error while compiling program:\n" <> pretty err
    Right ski -> pure ski
