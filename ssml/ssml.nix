{ mkDerivation, base, Comb, containers, haskeline, lens, mtl
, stdenv, text
}:
mkDerivation {
  pname = "SSML";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    base Comb containers haskeline lens mtl text
  ];
  executableHaskellDepends = [ base Comb haskeline lens mtl ];
  license = stdenv.lib.licenses.agpl3;
}
