use super::BFInstruction;

const BASE_ASSEMBLY: &str = "
.data  # start of data segment

tape:
    .fill 30000
pointer:
    .quad tape

.text  # start of code segment

.globl _start
_start:
            movq pointer, %rsi # move pointer to %rsi
";

const END_ASSEMBLY: &str = "
exit:       movq $60, %rax # _exit syscall
            movq $0, %rdi # error code 0
            syscall";

pub struct Compiler {
    brainfuck: Vec<BFInstruction>,
    jump_counter: usize,
}

impl Compiler {
    pub fn new(brainfuck: Vec<BFInstruction>) -> Compiler {
        Compiler {
            brainfuck,
            jump_counter: 0,
        }
    }

    // TERRIBLY inefficient for now
    pub fn compile(mut self) -> String {
        let mut code = self.slice_to_naive_assembly(&self.brainfuck.clone());

        code.insert_str(0, BASE_ASSEMBLY);
        code.push_str(END_ASSEMBLY);

        code
    }

    fn slice_to_naive_assembly(&mut self, instructions: &[BFInstruction]) -> String {
        let mut acc = String::new();

        for i in instructions {
            acc.push_str(&self.to_naive_assembly(i));
        }

        return acc;
    }

    fn to_naive_assembly(&mut self, instr: &BFInstruction) -> String {
        use BFInstruction::*;

        match instr {
            READ => "
            movq $0, %rax # read syscall
            #movq pointer, %rsi # read into pointers adress (always in %rsi so not needed)
            movq $0, %rdi # from stdin
            movq $1, %rdx # read only 1 character
            syscall"
                .to_string(),
            PRINT => "
            movq $1, %rax # write syscall
            movq $1, %rdi # to stdout
            #movq pointer, %rsi # write to pointers adress (always in %rsi so not needed)
            movq $1, %rdx # write only 1 character
            syscall"
                .to_string(),
            Move(amount) => {
                if *amount > 0 {
                    format!(
                        "
            addq ${}, %rsi # increment pointer",
                        amount
                    )
                } else if *amount < 0 {
                    format!(
                        "
            subq ${}, %rsi # decrement pointer",
                        amount.abs()
                    )
                } else {
                    // moving 0 is the same as not moving at all
                    String::new()
                }
            }
            INCR(amount) => format!(
                "
            movb (%rsi), %bl # move tape value to %bl
            addb ${}, %bl # increment tape value
            movb %bl, (%rsi) # update tape value from %bl",
                amount
            ),
            DECR(amount) => format!(
                "
            movb (%rsi), %bl # move tape value to %bl
            subb ${}, %bl # decrement tape value
            movb %bl, (%rsi) # update tape value from %bl",
                amount
            ),
            LOOP(instructions) => {
                let loop_id = self.jump_counter;
                self.jump_counter += 1;

                let loop_code = self.slice_to_naive_assembly(instructions);

                format!(
                    "
            movb (%rsi), %bl
            test %bl, %bl
            jz end_{0}
begin_{0}:{1}
            movb (%rsi), %bl
            test %bl, %bl
            jnz begin_{0}
end_{0}:",
                    loop_id, loop_code
                )
            }
        }
    }
}
