pub mod compiler;
mod parser;

pub use compiler::Compiler;
pub use parser::parse_brainfuck;

pub struct BFEnvironment<R = std::io::Stdin, W = std::io::Stdout> {
    pub data: Vec<u8>,
    pub pointer: usize,
    reader: R,
    writer: W,
}

impl<T, S> BFEnvironment<T, S> {
    pub fn current_cell(&mut self) -> &mut u8 {
        &mut self.data[self.pointer]
    }
}

impl<R: std::io::Read, W: std::io::Write> BFEnvironment<R, W> {
    pub fn new(reader: R, writer: W) -> BFEnvironment<R, W> {
        BFEnvironment {
            data: vec![0],
            pointer: 0,
            reader,
            writer,
        }
    }

    pub fn reader(&mut self) -> &mut R {
        &mut self.reader
    }
}

impl<R, W: std::io::Write> BFEnvironment<R, W> {
    pub fn writer(&self) -> &W {
        &self.writer
    }

    pub fn writer_mut(&mut self) -> &mut W {
        &mut self.writer
    }
}

impl<T, S: std::fmt::Debug> std::fmt::Debug for BFEnvironment<T, S> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        f.debug_struct("BFEnvironment")
            .field("data", &self.data)
            .field("pointer", &self.pointer)
            .field("writer", &self.writer)
            .finish()
    }
}

#[derive(Clone, Debug)]
pub enum BFInstruction {
    INCR(u8),
    DECR(u8),
    Move(isize),
    PRINT,
    READ,
    LOOP(Vec<BFInstruction>),
}

impl BFInstruction {
    /// Try to combine `rhs` into this instruction, return `Some(rhs)` if that's impossible
    pub fn combine(&mut self, rhs: BFInstruction) -> Option<BFInstruction> {
        use BFInstruction::*;

        match self {
            INCR(n) => {
                if let INCR(n_rhs) = rhs {
                    *n += n_rhs;
                    None
                } else if let DECR(n_rhs) = rhs {
                    *n -= n_rhs;
                    None
                } else {
                    Some(rhs)
                }
            }
            DECR(n) => {
                if let DECR(n_rhs) = rhs {
                    *n += n_rhs;
                    None
                } else if let INCR(n_rhs) = rhs {
                    *n -= n_rhs;
                    None
                } else {
                    Some(rhs)
                }
            }
            Move(n) => {
                if let Move(n_rhs) = rhs {
                    *n += n_rhs;
                    None
                } else {
                    Some(rhs)
                }
            }
            _ => Some(rhs),
        }
    }

    pub fn execute<R: std::io::Read, W: std::io::Write>(&self, env: &mut BFEnvironment<R, W>) {
        use BFInstruction::*;

        match &self {
            INCR(n) => *env.current_cell() = env.current_cell().wrapping_add(*n),
            DECR(n) => *env.current_cell() = env.current_cell().wrapping_sub(*n),
            Move(amount) => {
                let new_pointer = env.pointer as isize + amount;

                assert!(new_pointer >= 0);

                env.pointer = new_pointer as usize;

                // allocate extra cells if necessary
                if env.pointer >= env.data.len() {
                    let needed = 1 + env.pointer - env.data.len();
                    for _ in 0..needed {
                        env.data.push(0);
                    }
                }

                assert!(env.pointer < env.data.len());
            }
            PRINT => {
                let buffer = &[*env.current_cell()];

                env.writer_mut().write(buffer).unwrap();
                env.writer_mut().flush().unwrap();
            }
            READ => {
                let mut ans = [0];
                env.reader().read_exact(&mut ans).unwrap();

                *env.current_cell() = ans[0];
            }
            LOOP(instr) => {
                while *env.current_cell() != 0 {
                    for i in instr {
                        i.execute(env);
                    }
                }
            }
        }
    }
}

#[test]
fn test_parse_brainfuck() {
    let test_text = "+[-[<<[+[--->]-[<<<]]]>>>-]>-.---.>..>.<<<<-.<+.>>>>>.>.<<.<-.";

    let test_instr = parse_brainfuck(test_text).unwrap();
    let mut output = Vec::new();
    let mut test_env = BFEnvironment::new(std::io::stdin(), &mut output);

    for i in &test_instr {
        i.execute(&mut test_env);
    }

    assert_eq!(b"hello world", output.as_slice());
}
