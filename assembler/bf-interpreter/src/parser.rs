extern crate nom;

use super::BFInstruction;
use nom::branch::alt;
use nom::character::complete::{char, none_of};
use nom::combinator::{all_consuming, map};
use nom::multi::fold_many0;
use nom::sequence::delimited;
use nom::IResult;

fn parse_loop(input: &str) -> IResult<&str, BFInstruction> {
    let parser = delimited(char('['), parse_sequence, char(']'));

    let (rem, res) = parser(input)?;

    let instr = BFInstruction::LOOP(res);

    Ok((rem, instr))
}

fn parse_move(input: &str) -> IResult<&str, BFInstruction> {
    let left = map(char('<'), |_| BFInstruction::Move(-1));
    let right = map(char('>'), |_| BFInstruction::Move(1));

    alt((left, right))(input)
}

fn parse_others(input: &str) -> IResult<&str, BFInstruction> {
    let parse_incr = map(char('+'), |_| BFInstruction::INCR(1));
    let parse_decr = map(char('-'), |_| BFInstruction::DECR(1));
    let parse_print = map(char('.'), |_| BFInstruction::PRINT);
    let parse_read = map(char(','), |_| BFInstruction::READ);

    alt((parse_incr, parse_decr, parse_print, parse_read))(input)
}

fn parse_bf_token(input: &str) -> IResult<&str, BFInstruction> {
    alt((parse_move, parse_others, parse_loop))(input)
}

fn parse_non_bf_token(input: &str) -> IResult<&str, ()> {
    let parser = none_of("[]+-.,<>");
    map(parser, |_| ())(input)
}

/// Parses brainfuck AND non-brainfuck tokens
fn parse_token(input: &str) -> IResult<&str, Option<BFInstruction>> {
    let non_bf = map(parse_non_bf_token, |_| None);
    let bf = map(parse_bf_token, |a| Some(a));

    alt((non_bf, bf))(input)
}

fn parse_sequence(input: &str) -> IResult<&str, Vec<BFInstruction>> {
    fold_many0(
        parse_token,
        Vec::new(),
        |mut acc: Vec<BFInstruction>, maybe_bf| {
            if let Some(bf) = maybe_bf {
                if let Some(x) = acc.last_mut() {
                    if let Some(new) = x.combine(bf) {
                        acc.push(new);
                    }
                } else {
                    acc.push(bf);
                }
            }

            acc
        },
    )(input)
}

pub fn parse_brainfuck(
    input: &str,
) -> Result<Vec<BFInstruction>, nom::Err<(&str, nom::error::ErrorKind)>> {
    let (_, res) = all_consuming(parse_sequence)(input)?;

    Ok(res)
}
