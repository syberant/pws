use super::instructions::vector::BFVector;
use std::collections::HashMap;

enum Variable {
    SizedElem(usize),
    Vector(usize, BFVector),
}

impl Variable {
    pub fn place(&self) -> usize {
        use Variable::*;

        match self {
            SizedElem(n) => *n,
            Vector(n, ..) => *n,
        }
    }
}

#[derive(Debug)]
pub struct VecElem<'a> {
    pub head: &'a mut u8,
    pub cells: Vec<&'a mut u8>,
}

impl<'a> VecElem<'a> {
    pub fn new(head: &'a mut u8, cells: Vec<&'a mut u8>) -> VecElem<'a> {
        VecElem { head, cells }
    }

    pub fn clone_cells(&self) -> Vec<u8> {
        self.cells.iter().map(|n| **n).collect()
    }
}

pub struct Environment<R = std::io::Stdin, W = std::io::Stdout> {
    data: Vec<u8>,
    scopes: Vec<HashMap<String, Variable>>,
    reader: R,
    writer: W,
}

impl<R: std::io::Read, W: std::io::Write> Environment<R, W> {
    pub fn new(reader: R, writer: W) -> Environment<R, W> {
        Environment {
            data: vec![0],
            scopes: vec![HashMap::new()],
            reader,
            writer,
        }
    }

    pub fn get_reader(&mut self) -> &mut R {
        &mut self.reader
    }

    pub fn get_writer(&mut self) -> &mut W {
        &mut self.writer
    }

    pub fn execute_program(&mut self, instructions: &[super::instructions::Instruction]) {
        for i in instructions {
            i.execute(self).unwrap();
        }
    }
}

impl<T, S> Environment<T, S> {
    pub fn get_from_top(&self, addr: usize) -> u8 {
        self.data[self.data.len() - 1 - addr]
    }

    pub fn set_from_top(&mut self, addr: usize, number: u8) {
        let at = self.data.len() - 1 - addr;
        self.data[at] = number;
    }

    pub fn pop(&mut self) -> u8 {
        self.data.pop().unwrap()
    }

    pub fn pop_u16(&mut self) -> u16 {
        let low = self.pop();
        let high = self.pop();

        u16::from_be_bytes([high, low])
    }

    pub fn top(&mut self) -> &mut u8 {
        self.data.last_mut().unwrap()
    }

    pub fn push(&mut self, n: u8) {
        self.data.push(n);
    }

    pub fn push_u16(&mut self, n: u16) {
        let num = n.to_be_bytes();
        self.extend(&num);
    }

    pub fn extend(&mut self, slice: &[u8]) {
        self.data.extend_from_slice(slice);
    }

    pub fn declare_var(&mut self, name: String) {
        // This permits shadowing (reusing) names
        self.scopes
            .last_mut()
            .unwrap()
            .insert(name, Variable::SizedElem(self.data.len() - 1));
    }

    pub fn declare_vector(&mut self, name: String, vector: BFVector) {
        self.scopes
            .last_mut()
            .unwrap()
            .insert(name, Variable::Vector(self.data.len() - 1, vector));
    }

    pub fn get_bfvector(&self, name: &String) -> &BFVector {
        if let Variable::Vector(_, bfvec) = self.get_from_scopes(name) {
            return bfvec;
        } else {
            panic!("Variable `{}` is not a vector.", name)
        }
    }

    pub fn get_top_bfvector(&self) -> &BFVector {
        self.scopes
            .iter()
            .rev()
            .flatten()
            .find_map(|(_name, var)| {
                if let Variable::Vector(height, bfvec) = var {
                    let current_height = self.data.len() - 1;

                    if *height == current_height {
                        return Some(bfvec);
                    }
                }

                None
            })
            .unwrap()
    }

    pub fn vector_elems<'a>(&'a mut self, name: &String) -> Vec<VecElem> {
        let var = self.get_from_scopes(name);
        if let Variable::Vector(n, bfvec) = var {
            let elem_size = bfvec.element_size();
            let place = *n;
            // Function returning a VecElem
            fn get_elem<'a, I: Iterator<Item = &'a mut u8>>(
                iter: &mut I,
                elem_size: usize,
            ) -> Option<VecElem<'a>> {
                let mut cells = Vec::with_capacity(elem_size);
                for _i in 0..elem_size {
                    cells.push(iter.next()?);
                }
                cells.reverse();

                let head = iter.next()?;

                Some(VecElem::new(head, cells))
            };

            // NOTE: the `..place` means that it does NOT include the end
            let mut iter = self.data[..place].iter_mut().rev();
            let mut acc = Vec::new();

            while let Some(elem) = get_elem(&mut iter, elem_size) {
                if *elem.head == 0 {
                    // This is the begin
                    acc.push(elem);
                    break;
                } else {
                    acc.push(elem);
                }
            }

            // println!("Returning vector_elems from `{}`: {:?}", name, &acc);

            return acc;
        } else {
            panic!("Variable `{}` is not a vector.", name);
        }
    }

    fn get_from_scopes(&self, name: &String) -> &Variable {
        for s in self.scopes.iter().rev() {
            if let Some(var) = s.get(name) {
                return var;
            }
        }

        panic!("Unknown variable `{}`, did you declare it?", name)
    }

    pub fn get_var(&self, name: &String) -> u8 {
        self.get_var_offset(name, 0)
    }

    pub fn set_var(&mut self, name: &String, number: u8) {
        self.set_var_offset(name, 0, number);
    }

    pub fn get_var_offset(&self, name: &String, offset: usize) -> u8 {
        let var = self.get_from_scopes(name);

        return self.data[var.place() + offset];
    }

    pub fn set_var_offset(&mut self, name: &String, offset: usize, number: u8) {
        let var = self.get_from_scopes(name);
        let index = var.place() + offset;

        self.data[index] = number;
    }

    pub fn enter_scope(&mut self) {
        self.scopes.push(HashMap::new());
    }

    pub fn leave_scope(&mut self) {
        self.scopes.pop();
    }
}

impl<T, S, R, W: std::io::Write + PartialEq<S>> PartialEq<bf_interpreter::BFEnvironment<R, W>>
    for Environment<T, S>
{
    fn eq(&self, rhs: &bf_interpreter::BFEnvironment<R, W>) -> bool {
        let mut rhs_iter = rhs.data.iter();
        let is_data_same = self.data.iter().zip(&mut rhs_iter).all(|(&a, &b)| a == b);
        let is_rest_empty = rhs_iter.all(|&n| n == 0);

        let is_pointer_good = rhs.pointer == self.data.len() - 1;

        // TODO: this checks everything in a Vec while everything that was already in there hasn't changed.
        // Maybe don't check that again?
        let is_output_same = *rhs.writer() == self.writer;

        return is_data_same && is_rest_empty && is_pointer_good && is_output_same;
    }
}

impl<T, W: std::fmt::Debug> std::fmt::Debug for Environment<T, W> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        f.debug_struct("Environment")
            .field("data", &self.data)
            .field("writer", &self.writer)
            .finish()
    }
}
