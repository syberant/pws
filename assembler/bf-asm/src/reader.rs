use std::cell::RefCell;
use std::collections::VecDeque;
use std::rc::Rc;

pub struct ReaderFactory<R>(Rc<RefCell<TSplitReader<R>>>);

impl<R> ReaderFactory<R> {
    pub fn new(reader: R) -> ReaderFactory<R> {
        ReaderFactory(Rc::from(RefCell::from(TSplitReader::new(reader))))
    }

    pub fn new_reader(&mut self) -> TestReader<R> {
        TestReader::new(self.0.clone())
    }
}

struct TSplitReader<R> {
    input: R,
    streams: Vec<VecDeque<u8>>,
}

impl<R> TSplitReader<R> {
    fn new(input: R) -> TSplitReader<R> {
        TSplitReader {
            input,
            streams: Vec::new(),
        }
    }

    fn new_client(&mut self) -> usize {
        let id = self.streams.len();

        self.streams.push(VecDeque::new());

        return id;
    }
}

impl<R: std::io::Read> TSplitReader<R> {
    fn read(&mut self, id: usize) -> std::io::Result<u8> {
        assert!(self.streams.len() > id);

        if self.streams[id].len() == 0 {
            let mut arr = [0];
            self.input.read_exact(&mut arr)?;

            for s in &mut self.streams {
                s.push_back(arr[0]);
            }
        }
        
        Ok(self.streams[id].pop_front().unwrap())
    }
}

pub struct TestReader<R> {
    from: Rc<RefCell<TSplitReader<R>>>,
    id: usize,
}

impl<R> TestReader<R> {
    fn new(from: Rc<RefCell<TSplitReader<R>>>) -> TestReader<R> {
        let id = from.borrow_mut().new_client();

        TestReader { from, id }
    }
}

impl<R: std::io::Read> std::io::Read for TestReader<R> {
    fn read(&mut self, destination: &mut [u8]) -> Result<usize, std::io::Error> {
        if destination.len() == 0 {
            Ok(0)
        } else {
            let n = self.from.borrow_mut().read(self.id)?;

            destination[0] = n;

            Ok(1)
        }
    }
}

pub struct ErrorReader;

impl std::io::Read for ErrorReader {
    fn read(&mut self, _destination: &mut [u8]) -> std::io::Result<usize> {
        Err(std::io::Error::new(std::io::ErrorKind::Other, "The program was not supposed to read during the test."))
    }
}