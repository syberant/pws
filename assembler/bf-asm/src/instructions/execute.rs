use super::Instruction;
use crate::environment::Environment;

impl Instruction {
    pub fn execute<R: std::io::Read, W: std::io::Write>(
        &self,
        env: &mut Environment<R, W>,
    ) -> std::io::Result<()> {
        use Instruction::*;

        match self {
            CLR => *env.top() = 0,
            PUSH(n) => env.push(*n),
            POP => {
                env.pop();
            }
            READ => {
                *env.top() = {
                    let mut arr = [0];
                    env.get_reader().read_exact(&mut arr)?;

                    arr[0]
                }
            }
            PUT => {
                let buffer = &[*env.top()];
                env.get_writer().write(buffer)?;
                env.get_writer().flush()?;
            }
            GET(addr) => {
                env.push(0);
                MOV(*addr + 1, 0).execute(env)?;
            }
            SET(addr) => {
                MOV(0, *addr).execute(env)?;
                env.pop();
            }
            MOV(from, to) => {
                let n = env.get_from_top(*from);
                env.set_from_top(*to, n);
            }
            DUP => {
                let n = *env.top();
                env.push(n);
            }
            SWAP => {
                let sp = env.pop();
                let sp_1 = env.pop();
                env.push(sp);
                env.push(sp_1);
            }

            INCR8 => *env.top() += 1,
            DECR8 => *env.top() -= 1,
            SUB8 => {
                let sp = env.pop();
                *env.top() -= sp;
            }
            ADD8 => {
                let sp = env.pop();
                *env.top() += sp;
            }
            DIV8 => {
                let sp = env.pop();
                *env.top() /= sp;
            }
            MOD8 => {
                let sp = env.pop();
                *env.top() %= sp;
            }
            MUL8 => {
                let sp = env.pop();
                *env.top() *= sp;
            }

            INCR16 => {
                let num = env.pop_u16().wrapping_add(1);
                env.push_u16(num);
            }
            DECR16 => {
                let num = env.pop_u16().wrapping_sub(1);
                env.push_u16(num);
            }
            ADD16 => {
                let num1 = env.pop_u16();
                let num2 = env.pop_u16();

                let res = num1.wrapping_add(num2);

                env.push_u16(res);
            }
            MUL16 => {
                let num1 = env.pop_u16();
                let num2 = env.pop_u16();

                let res = num1.wrapping_mul(num2);

                env.push_u16(res);
            }

            EQ8 => {
                let sp = env.pop();
                let sp_1 = env.pop();

                env.push(if sp == sp_1 { 1 } else { 0 });
            }
            GT8 => {
                let sp = env.pop();
                let sp_1 = env.pop();
                env.push(if sp > sp_1 { 1 } else { 0 });
            }
            GE8 => {
                let sp = env.pop();
                let sp_1 = env.pop();
                env.push(if sp >= sp_1 { 1 } else { 0 });
            }
            LT8 => {
                let sp = env.pop();
                let sp_1 = env.pop();
                env.push(if sp < sp_1 { 1 } else { 0 });
            }
            LE8 => {
                let sp = env.pop();
                let sp_1 = env.pop();
                env.push(if sp <= sp_1 { 1 } else { 0 });
            }

            OR => {
                let sp = env.pop();
                let sp_1 = env.pop();

                let new = sp != 0 || sp_1 != 0;
                env.push(new as u8);
            }

            #[cfg(feature = "experimental-instructions")]
            IF(fst, snd) => {
                env.enter_scope();

                if *env.top() != 0 {
                    // True

                    // empty current cell
                    *env.top() = 0;

                    // The cell used for NOT
                    env.push(255);

                    for i in fst {
                        i.execute(env)?;
                    }
                } else {
                    // False

                    // The cell used for NOT
                    env.push(0);

                    for i in snd {
                        i.execute(env)?;
                    }
                }

                // Pop the input and the NOT cell
                env.pop();
                env.pop();

                env.leave_scope();
            }
            #[cfg(feature = "experimental-instructions")]
            WHILE(code) => {
                while *env.top() != 0 {
                    env.enter_scope();
                    for i in code {
                        i.execute(env)?;
                    }
                    env.leave_scope();
                }
                env.pop();
            }

            #[cfg(feature = "experimental-instructions")]
            DECLARE_VAR(name) => env.declare_var(name.clone()),
            #[cfg(feature = "experimental-instructions")]
            GET_VAR(name) => {
                let val = env.get_var(name);
                env.push(val);
            }
            #[cfg(feature = "experimental-instructions")]
            SET_VAR(name) => {
                let val = env.pop();
                env.set_var(name, val);
            }
            #[cfg(feature = "experimental-instructions")]
            GET_VAR_OFFSET(name, offset) => {
                let val = env.get_var_offset(name, *offset);
                env.push(val);
            }
            #[cfg(feature = "experimental-instructions")]
            SET_VAR_OFFSET(name, offset) => {
                let val = env.pop();
                env.set_var_offset(name, *offset, val);
            }

            #[cfg(feature = "arrays")]
            ARR_MAKE(len, elem_size, name) => {
                let mut empty_cells = Vec::with_capacity(elem_size + 1);
                for _i in 0..elem_size + 1 {
                    empty_cells.push(0);
                }

                // Push the beginning
                env.extend(&empty_cells);

                // Push the unused cells
                empty_cells[0] = 255;
                for _i in 0..*len {
                    env.extend(&empty_cells);
                }

                // Push the end
                env.push(0);

                // Declare this as a var
                env.declare_vector(name.clone(), super::vector::BFVector::new(*elem_size));
            }
            #[cfg(feature = "arrays")]
            ARR_DESTROY => {
                let top_vector = env.get_top_bfvector();
                let elem_size = top_vector.element_size();

                // Note: doesn't pop `head`
                let pop_elem = |environ: &mut Environment<R, W>| {
                    for _i in 0..elem_size {
                        environ.pop();
                    }
                };

                // Pop end
                assert_eq!(0, env.pop());

                // Middle
                // Should be a do-while loop but those are ugly in Rust
                pop_elem(env);
                while env.pop() != 0 {
                    pop_elem(env);
                }

                // Begin
                // Is popped at the end of the loop above
            }
            #[cfg(feature = "arrays")]
            ARR_PUSH(name) => {
                let elem_size = env.get_bfvector(name).element_size();
                let mut elem_from_top = Vec::with_capacity(elem_size);
                for _i in 0..elem_size {
                    let cell = env.pop();
                    elem_from_top.push(cell);
                }

                let mut vec = env.vector_elems(name);

                let mut iter = vec
                    .iter_mut()
                    .rev()
                    .skip(1)
                    .skip_while(|elem| *elem.head == 1);
                let elem = iter.next().expect("No unused cells in vector.");
                *elem.head = 1;
                for i in &mut elem.cells {
                    let cell = elem_from_top.pop().unwrap();
                    **i = cell;
                }
            }
            #[cfg(feature = "arrays")]
            ARR_POP(name) => {
                let mut vec = env.vector_elems(name);
                let mut iter = vec.iter_mut().skip_while(|elem| *elem.head != 1);
                let elem = iter.next().expect("No used cells in vector.");

                // Deselect
                *elem.head = 255;

                // Save cells
                let cells = elem.clone_cells();

                // Empty cells
                for i in &mut elem.cells {
                    **i = 0;
                }

                // Push cells to stack
                env.extend(&cells);
            }
            #[cfg(feature = "arrays")]
            ARR_INDEX(name) => {
                let index = env.pop();

                let mut vec = env.vector_elems(name);
                let elem = vec
                    .iter_mut()
                    .rev()
                    .skip(1)
                    .nth(index as usize)
                    .expect("Index is higher than length of vector.");
                *elem.head = 0;
            }
            #[cfg(feature = "arrays")]
            ARR_UNSEL(name) => {
                let mut vec = env.vector_elems(name);
                let elem = vec.iter_mut().rev().next().unwrap();

                *elem.head = 1;
            }
            #[cfg(feature = "arrays")]
            ARR_GET(name) => {
                let mut vec = env.vector_elems(name);
                let elem = vec.iter_mut().rev().next().unwrap();
                let cells = elem.clone_cells();

                env.extend(&cells);
            }

            other => unimplemented!("{:?}", other),
        }

        Ok(())
    }
}