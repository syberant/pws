pub mod vector;
mod brainfuck;
mod execute;

use super::compiler::state::StackTracker;

#[derive(Clone, Debug, PartialEq)]
#[allow(non_camel_case_types)]
pub enum Instruction {
    CLR,
    PUSH(u8),
    POP,
    READ,
    PUT,
    GET(usize),
    SET(usize),
    MOV(usize, usize),
    DUP,
    SWAP,

    INCR8,
    DECR8,
    SUB8,
    ADD8,
    DIV8,
    MOD8,
    MUL8,

    INCR16,
    DECR16,
    ADD16,
    MUL16,

    EQ8,
    GT8,
    GE8,
    LT8,
    LE8,

    XOR,
    AND,
    OR,
    NOT,

    #[cfg(feature = "experimental-instructions")]
    IF(Vec<Instruction>, Vec<Instruction>),
    #[cfg(feature = "experimental-instructions")]
    WHILE(Vec<Instruction>),

    #[cfg(feature = "experimental-instructions")]
    DECLARE_VAR(String),
    #[cfg(feature = "experimental-instructions")]
    GET_VAR(String),
    #[cfg(feature = "experimental-instructions")]
    SET_VAR(String),
    #[cfg(feature = "experimental-instructions")]
    GET_VAR_OFFSET(String, usize),
    #[cfg(feature = "experimental-instructions")]
    SET_VAR_OFFSET(String, usize),

    #[cfg(feature = "arrays")]
    /// len, elem_size, name
    ARR_MAKE(usize, usize, String),
    #[cfg(feature = "arrays")]
    ARR_DESTROY,
    #[cfg(feature = "arrays")]
    ARR_INDEX(String),
    #[cfg(feature = "arrays")]
    ARR_GET(String),
    #[cfg(feature = "arrays")]
    ARR_PUSH(String),
    #[cfg(feature = "arrays")]
    ARR_POP(String),
    #[cfg(feature = "arrays")]
    ARR_UNSEL(String),
}

impl Instruction {
    #[cfg(feature = "experimental-instructions")]
    pub fn slice_stack_influence(prog: &[Instruction], comp: &mut super::Compiler) -> isize {
        let mut counter = 0;

        for i in prog {
            use Instruction::{ARR_DESTROY, ARR_MAKE};

            match i {
                ARR_MAKE(_len, elem_size, name) => {
                    // println!("Declaring `{}` as a vector with {} elements of size {}.", name, _len, elem_size);

                    let bfvec = vector::BFVector::new(*elem_size);
                    comp.stack_height.add_vector_stack_height(bfvec);
                    comp.vars.declare_var(name.clone(), &comp.stack_height);
                }
                ARR_DESTROY => {
                    use super::compiler::state::StackElem;

                    let top = comp.stack_height.get_elems().last().unwrap();
                    if let StackElem::Vector(v) = top {
                        // Make a copy here so the BC doesn't get mad
                        let bfvec = v.clone();

                        // Remove this vector from the compiler's view of the stack
                        comp.stack_height.sub_vector_stack_height(bfvec);
                    } else {
                        panic!("ARR_DESTROY expected a vector, got {:?}.", top);
                    }
                }
                _ => counter += i.stack_influence(&comp),
            }
        }

        return counter;
    }

    #[cfg(feature = "experimental-instructions")]
    fn stack_influence(&self, comp: &super::compiler::Compiler) -> isize {
        use Instruction::*;

        match self {
            CLR => 0,
            PUSH(..) => 1,
            POP => -1,
            READ => 0,
            PUT => 0,
            GET(..) => 1,
            SET(..) => -1,
            MOV(..) => 0,
            DUP => 1,
            SWAP => 0,

            INCR8 => 0,
            DECR8 => 0,
            SUB8 => -1,
            ADD8 => -1,
            DIV8 => -1,
            MOD8 => -1,
            MUL8 => -1,

            INCR16 => 0,
            DECR16 => 0,
            ADD16 => -2,
            MUL16 => -2,

            EQ8 => -1,
            GT8 => -1,
            GE8 => -1,
            LT8 => -1,
            LE8 => -1,

            XOR => -1,
            AND => -1,
            OR => -1,
            NOT => -1,

            #[cfg(feature = "experimental-instructions")]
            IF(..) => -1,
            #[cfg(feature = "experimental-instructions")]
            WHILE(..) => -1,
            #[cfg(feature = "experimental-instructions")]
            DECLARE_VAR(..) => 0,
            #[cfg(feature = "experimental-instructions")]
            GET_VAR(..) => 1,
            #[cfg(feature = "experimental-instructions")]
            SET_VAR(..) => -1,
            #[cfg(feature = "experimental-instructions")]
            GET_VAR_OFFSET(..) => 1,
            #[cfg(feature = "experimental-instructions")]
            SET_VAR_OFFSET(..) => -1,

            #[cfg(feature = "arrays")]
            ARR_MAKE(..) => 0, // Vector is pushed onto the stack elsewhere
            #[cfg(feature = "arrays")]
            ARR_DESTROY => 0, // Vector is popped from the stack elsewhere
            #[cfg(feature = "arrays")]
            ARR_INDEX(..) => -1,
            #[cfg(feature = "arrays")]
            ARR_GET(name) => {
                let vec = comp.vars.get_vector_var(&name);
                vec.element_size() as isize
            }
            #[cfg(feature = "arrays")]
            ARR_PUSH(name) => {
                let vec = comp.vars.get_vector_var(&name);
                -(vec.element_size() as isize)
            }
            #[cfg(feature = "arrays")]
            ARR_POP(name) => {
                let vec = comp.vars.get_vector_var(&name);
                vec.element_size() as isize
            }
            #[cfg(feature = "arrays")]
            ARR_UNSEL(..) => 0,
        }
    }
}
