use super::Instruction;
use crate::compiler::state::StackTracker;

impl Instruction {
    pub fn get_brainfuck(&self, compiler: &mut super::super::compiler::Compiler) -> String {
        use super::super::compiler::state::StackElem;
        use Instruction::*;

        let res = match &self {
            CLR => "[-]".to_string(),
            PUSH(n) => format!(">{}", "+".repeat(*n as usize)),
            POP => "[-]<".to_string(),
            READ => ",".to_string(),
            PUT => ".".to_string(),
            GET(n) => filter_brainfuck(&format!(
                ">
{0}     ga naar cel a
[         zolang a groter is dan 0
  -       trek 1 van a af
  {1}   ga naar %sp
  +       tel 1 bij %sp op
  >       ga naar %sp1
  +       tel 1 bij %sp1 op
  {0} < ga naar cel a
]
{1} >   ga naar %sp1
[         zolang %sp1 groter is dan 0
  -       trek 1 van %sp1 af
  < {0} ga naar cel a
  +       tel 1 bij cel a op
  {1} > ga naar %sp1
]
<         ga naar %sp",
                "<".repeat(*n + 1),
                ">".repeat(*n + 1)
            )),
            SET(n) => filter_brainfuck(&format!(
                "{0} ga naar cel a
[-]       maak cel a leeg
{1}        ga naar %sp
[         zolang %sp is groter dan 0
  -       trek 1 van %sp af
  {0}   ga naar cel a
  +       tel 1 bij %sp op
  {1}   ga naar %sp
]<",
                "<".repeat(*n),
                ">".repeat(*n)
            )),
            SWAP => filter_brainfuck(
                "
// verplaats a naar derde cel
[->+<]

// verplaats b naar a
<[->+<]

// verplaats derde cel (=a) naar b
>>[-<<+>>]

// ga naar bovenkant stack
<
            ",
            ),
            DUP => "[->+>+<<]>>[-<<+>>]<".to_string(),

            INCR8 => "+".to_string(),
            DECR8 => "-".to_string(),
            SUB8 => "[-<->]<".to_string(),
            ADD8 => "[-<+>]<".to_string(),
            DIV8 => {
                // TODO: work this out instead of copy-pasting from the internet
                "[>>>+<<<-]<[->+>>+>-[<-]<[->>+<<<<[->>>+<<<]>]<<]>>>>[-]>[<<<<<+>>>>>-]<<<<[-]<"
                    .to_string()
            }
            MOD8 => {
                // TODO: work this out instead of copy-pasting from the internet
                "[>>>+<<<-]<[->+>>+>-[<-]<[->>+<<<<[->>>+<<<]>]<<]>>>>[-]>[<<<<<+>>>>>-]<<<<<[-]>[-<+>]<".to_string()
            }
            MUL8 => filter_brainfuck(
                "
<[
  // trek 1 af van b
  -
  // ga naar a
  >
  
  // tel a op bij derde en vierde cel
  [->+>+<<]
  // verplaats derde cel (=a) naar a
  >[-<+>]
  
  // ga naar b
  <<
]

// leeg a
>[-]

// kopieer vierde cel naar b (=output)
>>[-<<<+>>>]

// ga naar b
<<<
            ",
            ),

            INCR16 => filter_brainfuck(
                "
incr lower
+

set 5 to 1
>>>+<<<

these lines (efficiently) set 3 to 1 if 2 != 0 and move to 5
[>+>]
# p: 2 || p: 4
>-[+>-]
# p: 5

4 = NOT 3 
<+<[->-<]
# p: 3

if 4 then incr 1
>[-<<<+>>>]

go to 2
<<",
            ),
            DECR16 => filter_brainfuck(
                "
set 5 to 1
>>>+<<<

these lines (efficiently) set 3 to 1 if 2 != 0 and move to 5
[>+>]
# p: 2 || p: 4
>-[+>-]
# p: 5

4 = NOT 3 
<+<[->-<]
# p: 3

if 4 then decr 1
>[-<<<->>>]

go to 2
<<

decr 2
-",
            ),
            ADD16 => filter_brainfuck(
                "
add big part to other big part
<[<<+>>-]
# p: 3

>[-<+<+
	incr other small part (without touching the top one)
    
    [>->>]
    if 2 is 0 then 3 = 1 else 0
    >[>>>]
    # p: 6
    
    <<<
    # p: 3
    
    if 3 is 1 (so 2 == 0) then incr 1
    [<<+>>-]
>]
# p: 4

goto 2
<<",
            ),
            MUL16 => filter_brainfuck("
<<[->>>+>+<<<<]>>>>[-<<<<+>>>>]<><<<[->>>+<<<]>>[->[->+>+<<]>[-<+>]<<]>[
-]>>[-<<<+>>>]<<<><<<<<[->>>>>+<<<<<]>>>[->>>+>+<<<<]>>>>[-<<<<+>>>>]<<[->[->+>+<<]>[-<+>]<<]>[-]>>[-<<<+>>>]<<<[-<+>]<><<<<[->>>>+<<<
<]>>>>><<<[->>>+<<<]>>>

<[->[->>>>+>>>+<<<<<<<]
>>>
# p: 3

>[-<+<+
	incr other small part (without touching the top one)
    
    [>->>]
    if 2 is 0 then 3 = 1 else 0
    >[>>>]
    # p: 6
    
    <<<
    # p: 3
    
    if 3 is 1 (so 2 == 0) then incr 1
    [<<+>>-]
>]
# p: 4
>>>[-<<<<<<<+>>>>>>>]

<<<<<<<<
]

>[-]
>[-<<<<<<<+>>>>>>>]
>[-<<<<<<<+>>>>>>>]

<<<<
[-<<<<+>>>>]
<<<         "),

            EQ8 => filter_brainfuck(
                "<a
[>b-<a-]     trek a van b af
+            zet a naar waar
>b[<a->b[-]] zet a onwaar als b niet nul is
<a",
            ),

            OR => filter_brainfuck("
// ga naar a
<

[[-]>>+<<]>  // tel 1 bij derde cel op als a is niet nul

[[-]>+<]> // tel 1 bij derde cel op als b is niet nul

// als derde cel is nul dan false anders true
[[-] <<+>>]

// ga naar a (wordt gebruikt als return)
<<          "),

            #[cfg(feature = "experimental-instructions")]
            IF(fst, snd) => {
                // Add the NOT cell
                compiler.stack_height.add_stack_height(1);

                // Compile the blocks
                let fst_bf = compiler.compile_scope(fst);
                let snd_bf = compiler.compile_scope(snd);

                // Remove the NOT cell
                compiler.stack_height.sub_stack_height(1);

                format!("[[-]>-{}<]>+[-{}]<<", fst_bf, snd_bf)
            }
            #[cfg(feature = "experimental-instructions")]
            WHILE(code) => {
                let code_bf = compiler.compile_scope(code);

                format!("[{}]<", code_bf)
            }

            #[cfg(feature = "experimental-instructions")]
            DECLARE_VAR(name) => {
                compiler.vars.declare_var(name.clone(), &compiler.stack_height);

                // Return nothing
                String::new()
            }
            #[cfg(feature = "experimental-instructions")]
            GET_VAR(name) => {
                let temp = GET_VAR_OFFSET(name.clone(), 0).get_brainfuck(compiler);

                // That unjustly increased the stack height by 1 so manually correct it
                compiler.stack_height.sub_stack_height(1);

                temp
            }
            #[cfg(feature = "experimental-instructions")]
            SET_VAR(name) => {
                let temp = SET_VAR_OFFSET(name.clone(), 0).get_brainfuck(compiler);

                // That unjustly decreased the stack height by 1 so manually correct it
                compiler.stack_height.add_stack_height(1);

                temp
            }
            #[cfg(feature = "experimental-instructions")]
            GET_VAR_OFFSET(name, offset) => {
                let top = &compiler.stack_height;
                let mut goal = compiler.vars.get_height_var(name).clone();
                goal.push_sized(*offset);

                if top == &goal {
                    let temp = DUP.get_brainfuck(compiler);

                    // That unjustly increased the stack height by 1 so manually correct it
                    compiler.stack_height.sub_stack_height(1);

                    temp
                } else {
                    format!(
                        "{0}[-{1}>+>+<<{0}]{1}>>[-<<{0}+>>{1}]<",
                        top.move_to(&goal),
                        top.move_back_from(&goal)
                    )
                }
            }
            #[cfg(feature = "experimental-instructions")]
            SET_VAR_OFFSET(name, offset) => {
                let top = &compiler.stack_height;
                let mut goal = compiler.vars.get_height_var(name).clone();
                goal.push_sized(*offset);

                if top == &goal {
                    panic!("Can't set the current cell you fool!")
                } else {
                    format!(
                        "{0}[-]{1}[-{0}+{1}]<",
                        top.move_to(&goal),
                        top.move_back_from(&goal)
                    )
                }
            }
            #[cfg(feature = "arrays")]
            ARR_MAKE(len, elem_size, name) => {
                let bfvec = super::vector::BFVector::new(*elem_size);

                let res = bfvec.bf_create_empty(*len);

                compiler.stack_height.add_vector_stack_height(bfvec);
                compiler.vars.declare_var(name.to_string(), &compiler.stack_height);

                res
            }
            #[cfg(feature = "arrays")]
            ARR_DESTROY => {
                let top = compiler.stack_height.get_elems().last().unwrap();
                if let StackElem::Vector(v) = top {
                    let res = v.bf_destroy_vector();

                    // Make a copy at this moment because the BC gets mad otherwise
                    let bfvec = v.clone();

                    // Remove this vector from the compiler's view of the stack
                    compiler.stack_height.sub_vector_stack_height(bfvec);

                    res
                } else {
                    unreachable!()
                }
            }
            #[cfg(feature = "arrays")]
            ARR_INDEX(name) => {
                let top = &compiler.stack_height;
                let goal = compiler.vars.get_height_var(name);
                if let StackElem::Vector(v) = goal.get_elems().last().unwrap() {
                    v.bf_index_8(top.move_to(goal), top.move_back_from(goal))
                } else {
                    unreachable!()
                }
            }
            #[cfg(feature = "arrays")]
            ARR_PUSH(name) => {
                let top = &compiler.stack_height;
                let goal = compiler.vars.get_height_var(name);

                let bfvec = compiler.vars.get_vector_var(name);

                let select_new = format!(
                    "{}{}{}",
                    top.move_to(goal),
                    bfvec.bf_select_first_unused(),
                    top.move_back_from(goal)
                );
                let set_to_selected =
                    bfvec.bf_set_to_selected(top.move_to(goal), top.move_back_from(goal));
                let deselect = format!(
                    "{}{}{}{}{}",
                    ">".repeat(bfvec.element_size()),
                    top.move_to(goal),
                    bfvec.bf_deselect(),
                    top.move_back_from(goal),
                    "<".repeat(bfvec.element_size())
                );

                format!("{}{}{}", select_new, set_to_selected, deselect)
            }
            #[cfg(feature = "arrays")]
            ARR_POP(name) => {
                let top = &compiler.stack_height;
                let goal = compiler.vars.get_height_var(name);

                let bfvec = compiler.vars.get_vector_var(name);

                let select_last = format!(
                    "{}{}{}",
                    top.move_to(goal),
                    bfvec.bf_select_last_used(),
                    top.move_back_from(goal)
                );
                // Getting should erase the cell here
                let get_from_selected =
                    bfvec.bf_get_from_selected(top.move_to(goal), top.move_back_from(goal));
                let deselect = format!(
                    "{}{}{}{}{}",
                    "<".repeat(bfvec.element_size()),
                    top.move_to(goal),
                    bfvec.bf_deselect_to_unused(),
                    top.move_back_from(goal),
                    ">".repeat(bfvec.element_size())
                );

                format!("{}{}{}", select_last, get_from_selected, deselect)
            }
            #[cfg(feature = "arrays")]
            ARR_UNSEL(name) => {
                let top = &compiler.stack_height;
                let goal = compiler.vars.get_height_var(name);
                let bfvec = compiler.vars.get_vector_var(name);

                format!(
                    "{}{}{}",
                    top.move_to(goal),
                    bfvec.bf_deselect(),
                    top.move_back_from(goal)
                )
            }
            #[cfg(feature = "arrays")]
            ARR_GET(name) => {
                let _bfvec = compiler.vars.get_vector_var(name);
                unimplemented!()
            }

            other => unimplemented!("{:?}", other),
        };

        // Update compiler
        let diff = self.stack_influence(compiler);

        if diff > 0 {
            compiler.stack_height.add_stack_height(diff as usize);
        } else {
            compiler.stack_height.sub_stack_height(diff.abs() as usize);
        }

        // Return the brainfuck code
        return res;
    }
}


fn filter_brainfuck(input: &str) -> String {
    input
        .chars()
        .filter(|c| match c {
            '<' | '>' | '+' | '-' | '[' | ']' | '.' | ',' => true,
            _ => false,
        })
        .collect()
}