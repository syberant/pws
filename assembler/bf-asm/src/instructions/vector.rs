#[derive(Clone, Debug, PartialEq)]
pub struct BFVector {
    element_size: usize,
}

// Everything for executing
impl BFVector {}

// Everything for compiling to brainfuck
impl BFVector {
    /// Creates a vector with capacity len
    pub fn bf_create_empty(&self, len: usize) -> String {
        let create_elems = format!("-{0}", self.move_to_next_elem()).repeat(len);

        format!(">{0}{1}", self.move_to_next_elem(), create_elems)
    }

    /// Generates code to clear the entire vector
    pub fn bf_destroy_vector(&self) -> String {
        format!("{0}[{0}]<", self.empty_elem_from_next_and_destroy_head())
    }

    /// Generate code to index (8 bit)
    pub fn bf_index_8(&self, to_vector: String, to_top: String) -> String {
        let select_first = self.select_first_elem();
        let select_next = self.select_next_elem_from_end();

        format!(
            "{0}{2}{3}[-{0}{1}{3}]<",
            to_vector, select_next, select_first, to_top
        )
    }

    pub fn bf_select_first_unused(&self) -> String {
        let goto_begin = self.from_end_to_begin();
        let find_new = self.find_255_to_right();
        let goto_end = self.find_zero_to_right();

        format!("{}{}{}", goto_begin, find_new, goto_end)
    }

    pub fn bf_select_last_used(&self) -> String {
        let select = self.find_one_to_left();
        let goto_begin = self.find_zero_to_right();

        format!("{}{}", select, goto_begin)
    }

    pub fn bf_deselect(&self) -> String {
        self.deselect_from_end()
    }

    pub fn bf_deselect_to_unused(&self) -> String {
        self.deselect_to_unused_from_end()
    }

    /// Pops `element_size` cells from the stack and adds them to the selected element (you might want to empty it first).
    /// NOTE: does not deselect the element.
    pub fn bf_set_to_selected(&self, to_vector: String, to_top: String) -> String {
        let mut acc = String::new();

        for element in 0..self.element_size {
            let rev_element = self.element_size - element - 1;
            let to_top_cell = format!(
                "<{3}{2}{0}{1}",
                to_top,
                "<".repeat(element),
                self.find_zero_to_right(),
                "<".repeat(rev_element)
            );
            let to_vec_cell = format!(
                "{0}{1}{2}>{3}",
                ">".repeat(element),
                to_vector,
                self.find_zero_to_left(),
                ">".repeat(rev_element)
            );

            let set_elem = format!("[-{}+{}]<", to_vec_cell, to_top_cell);

            acc.push_str(&set_elem);
        }

        // This is functionality for deselecting the element as well, I will leave it here for reference purposes but it is not meant to be used with the current architecture.
        // let deselect_elem = format!("{}{}{}{}{}", to_vector, ">".repeat(self.element_size), self.deselect_from_end(), to_top, "<".repeat(self.element_size));
        // acc.push_str(&deselect_elem);

        return acc;
    }

    /// Empties the selected element and pushes its cells onto the stack.
    /// NOTE: does not deselect the element.
    pub fn bf_get_from_selected(&self, to_vector: String, to_top: String) -> String {
        let mut acc = String::new();

        for element in 0..self.element_size {
            let to_top_cell = format!(
                "<{1}{2}{0}{3}>",
                to_top,
                "<".repeat(element),
                self.find_zero_to_right(),
                ">".repeat(element)
            );
            let to_vec_cell = format!(
                "<{3}{1}{2}>{0}",
                ">".repeat(element),
                to_vector,
                self.find_zero_to_left(),
                "<".repeat(element)
            );

            let set_elem = format!("[-{1}+{0}]>", to_vec_cell, to_top_cell);

            if element == 0 {
                // First cell
                acc.push('>');
                acc.push_str(&to_vec_cell);
            }

            acc.push_str(&set_elem);

            if element == self.element_size - 1 {
                // Last cell
                acc.push('<');
                acc.push_str(&to_top_cell);
            }
        }

        return acc;
    }
}

// Basic building blocks
impl BFVector {
    fn move_to_next_elem(&self) -> String {
        ">".repeat(self.element_size + 1)
    }

    fn move_to_prev_elem(&self) -> String {
        "<".repeat(self.element_size + 1)
    }

    /// Empties element where pointer is and moves to next element
    fn empty_elem_and_move_next(&self) -> String {
        format!("{}>", ">[-]".repeat(self.element_size))
    }

    /// Empties previous element and moves pointer to the "head" of that element
    fn empty_elem_from_next_and_destroy_head(&self) -> String {
        format!("{}", "[-]<".repeat(self.element_size + 1))
    }

    /// Stops when it encounters an element with 0 (or the beginning), goes left
    fn find_zero_to_left(&self) -> String {
        format!("{0}[{0}]", self.move_to_prev_elem())
    }

    /// Stops when it encounters an element with 0 (or the end), goes right
    fn find_zero_to_right(&self) -> String {
        format!("{0}[{0}]", self.move_to_next_elem())
    }

    /// Stops when it encounters an element with 255 and selects it, goes right
    fn find_255_to_right(&self) -> String {
        // Should be this
        // format!("{0}+[-{0}+]", self.move_to_next_elem())

        // But this is more generic
        self.find_n_to_right(255)
    }

    fn find_one_to_left(&self) -> String {
        format!("{0}-[+{0}-]", self.move_to_prev_elem())
    }

    fn find_n_to_right(&self, n: u8) -> String {
        let (to_zero, restore);
        if n < 128 {
            to_zero = "-".repeat(n as usize);
            restore = "+".repeat(n as usize);
        } else {
            // 0 - 255 == 256 - 255 en het aantal wat je de andere kant op moet
            to_zero = "+".repeat(0_u8.wrapping_sub(n) as usize);
            restore = "-".repeat(0_u8.wrapping_sub(n) as usize);
        }

        format!(
            "{2}{0}[{1}{2}{0}]",
            to_zero,
            restore,
            self.move_to_next_elem()
        )
    }

    fn select_next_elem_from_end(&self) -> String {
        let find_selected_elem = self.find_zero_to_left();
        let return_to_end = self.find_zero_to_right();

        format!(
            "{}+{}-{}",
            find_selected_elem,
            self.move_to_next_elem(),
            return_to_end
        )
    }

    fn select_256_next_elem_from_end(&self) -> String {
        let find_selected_elem = self.find_zero_to_left();
        let jump_elements = self.move_to_next_elem().repeat(256);
        let return_to_end = self.find_zero_to_right();

        format!("{}+{}-{}", find_selected_elem, jump_elements, return_to_end)
    }

    fn deselect_from_end(&self) -> String {
        let find_selected_elem = self.find_zero_to_left();
        let return_to_end = self.find_zero_to_right();

        format!("{}+{}", find_selected_elem, return_to_end)
    }

    fn deselect_to_unused_from_end(&self) -> String {
        let find_selected_elem = self.find_zero_to_left();
        let return_to_end = self.find_zero_to_right();

        format!("{}-{}", find_selected_elem, return_to_end)
    }

    fn select_first_elem(&self) -> String {
        let find_selected_elem = self.find_zero_to_left();
        let return_to_end = self.find_zero_to_right();

        format!(
            "{}{}-{}",
            find_selected_elem,
            self.move_to_next_elem(),
            return_to_end
        )
    }

    /// Pointer is at the end, move it to the beginning
    pub fn from_end_to_begin(&self) -> String {
        self.find_zero_to_left()
    }

    /// Pointer is at the beginning, move it to the end
    pub fn from_begin_to_end(&self) -> String {
        self.find_zero_to_right()
    }
}

// Utility functions
impl BFVector {
    pub fn element_size(&self) -> usize {
        self.element_size
    }

    pub fn new(element_size: usize) -> BFVector {
        BFVector { element_size }
    }
}
