macro_rules! one_of {
    ( $input:ident, $parser:ident ) => {
        $parser($input)
    };
    ( $input:ident, $head:ident, $($rest:ident),+) => {
        match $head($input) {
            Ok(res) => Ok(res),
            Err(_) => one_of!( $input, $($rest),+ ),
        }
    };
}

#[macro_export]
macro_rules! all_instr {
    (
        empty  [ $($e_instr:ident),+ ]
        string [ $($s_instr:ident),+ ]
        number [ $($n_instr:ident),+ ]
        manual [ $($m_instr:ident),+ ]
    ) => {
        empty_instr!($($e_instr),+);
        string_instr!($($s_instr),+);
        number_instr!($($n_instr),+);

        fn parse_instr(input: &str) -> IResult<&str, Instruction> {
            use manual::*;

            one_of!(
                input,
                $($e_instr),+ ,
                $($s_instr),+ ,
                $($n_instr),+ ,
                $($m_instr),+
            )
        }
    };
}

macro_rules! empty_instr {
    ( $name:ident ) => {
        #[allow(non_snake_case)]
        fn $name(input: &str) -> IResult<&str, Instruction> {
            let (remaining, _) = tag(stringify!($name))(input)?;

            let instr = Instruction::$name;

            Ok((remaining, instr))
        }
    };

    ( $name:ident, $($other_name:ident),+ ) => {
        empty_instr!($name);
        empty_instr!($($other_name),+);
    };
}

macro_rules! string_instr {
    ( $name:ident ) => {
        #[allow(non_snake_case)]
        fn $name(input: &str) -> IResult<&str, Instruction> {
            let tag_and_whitespace = plus_ws!(tag(stringify!($name)));
            let (remaining, string) = preceded(tag_and_whitespace, alphanumeric1)(input)?;

            let instr = Instruction::$name(string.to_string());

            Ok((remaining, instr))
        }
    };
    ( $name:ident, $($other_name:ident),+ ) => {
        string_instr!($name);
        string_instr!($($other_name),+);
    };
}

macro_rules! number_instr {
    ( $name:ident ) => {
        #[allow(non_snake_case)]
        fn $name(input: &str) -> IResult<&str, Instruction> {
            let tag_and_whitespace = plus_ws!(tag(stringify!($name)));
            let (remaining, number) = preceded(tag_and_whitespace, parse_number)(input)?;

            let instr = Instruction::$name(number);

            Ok((remaining, instr))
        }
    };
    ( $name:ident, $($other_name:ident),+ ) => {
        number_instr!($name);
        number_instr!($($other_name),+);
    };
}

/// Parse (and discard) a section of whitespace following the input parser
#[macro_export]
macro_rules! plus_ws {
    ($parser:expr) => {
        terminated($parser, multispace1)
    };
}
