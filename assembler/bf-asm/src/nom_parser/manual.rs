use super::{
    alphanumeric1, multispace1, parse_block, parse_number, preceded, tag, terminated, tuple,
    IResult, Instruction,
};

#[allow(non_snake_case)]
pub fn ARR_MAKE(input: &str) -> IResult<&str, Instruction> {
    let parse_tag = plus_ws!(tag("ARR_MAKE"));
    let parse_name = plus_ws!(preceded(parse_tag, alphanumeric1));
    let parse_elem_size = plus_ws!(parse_number);
    let parse_len = parse_number;

    let (remaining, (name, elem_size, len)) =
        tuple((parse_name, parse_elem_size, parse_len))(input)?;
    // NOTE: copying the data here to form a String
    let instr = Instruction::ARR_MAKE(len, elem_size, name.to_string());
    Ok((remaining, instr))
}

#[test]
fn test_parse_arr_make() {
    let test_data = "ARR_MAKE testvector 2 6";
    let (rem, res) = ARR_MAKE(test_data).unwrap();

    assert_eq!(res, Instruction::ARR_MAKE(6, 2, "testvector".to_string()));
    assert!(rem.is_empty());
}

#[allow(non_snake_case)]
pub fn WHILE(input: &str) -> IResult<&str, Instruction> {
    let parse_tag = plus_ws!(tag("WHILE"));
    let (rem, output) = preceded(parse_tag, parse_block)(input)?;

    let instr = Instruction::WHILE(output);

    Ok((rem, instr))
}

#[allow(non_snake_case)]
pub fn IF(input: &str) -> IResult<&str, Instruction> {
    let parse_tag = plus_ws!(tag("IF"));
    let parser = preceded(parse_tag, tuple((plus_ws!(parse_block), parse_block)));

    let (rem, (fst, snd)) = parser(input)?;
    let instr = Instruction::IF(fst, snd);

    Ok((rem, instr))
}

#[allow(non_snake_case)]
pub fn SET_VAR_OFFSET(input: &str) -> IResult<&str, Instruction> {
    let parse_tag = plus_ws!(tag("SET_VAR_OFFSET"));
    let parse_string = plus_ws!(alphanumeric1);
    let parser = preceded(parse_tag, tuple((parse_string, parse_number)));

    let (rem, (name, num)) = parser(input)?;
    let instr = Instruction::SET_VAR_OFFSET(name.to_string(), num);

    Ok((rem, instr))
}

#[allow(non_snake_case)]
pub fn GET_VAR_OFFSET(input: &str) -> IResult<&str, Instruction> {
    let parse_tag = plus_ws!(tag("GET_VAR_OFFSET"));
    let parse_string = plus_ws!(alphanumeric1);
    let parser = preceded(parse_tag, tuple((parse_string, parse_number)));

    let (rem, (name, num)) = parser(input)?;
    let instr = Instruction::GET_VAR_OFFSET(name.to_string(), num);

    Ok((rem, instr))
}

#[allow(non_snake_case)]
pub fn MOV(input: &str) -> IResult<&str, Instruction> {
    let parse_tag = plus_ws!(tag("MOV"));
    let parser = preceded(parse_tag, tuple((plus_ws!(parse_number), parse_number)));

    let (rem, (fst, snd)) = parser(input)?;
    let instr = Instruction::MOV(fst, snd);

    Ok((rem, instr))
}
