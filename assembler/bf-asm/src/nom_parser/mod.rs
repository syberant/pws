extern crate nom;

#[macro_use]
mod macros;
mod manual;

use super::Instruction;
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::{
    alphanumeric1, char, digit1, line_ending, multispace0, multispace1, not_line_ending,
};
use nom::combinator::{all_consuming, map, opt, peek};
use nom::multi::fold_many0;
use nom::sequence::{delimited, preceded, terminated, tuple};
use nom::IResult;

all_instr!(
    empty [
        CLR,
        POP,
        READ,
        PUT,
        DUP,
        SWAP,
        INCR8,
        DECR8,
        SUB8,
        ADD8,
        DIV8,
        MOD8,
        MUL8,
        INCR16,
        DECR16,
        ADD16,
        MUL16,
        EQ8,
        GT8,
        GE8,
        LT8,
        LE8,
        XOR,
        AND,
        OR,
        NOT,
        ARR_DESTROY
    ]

    string [
        DECLARE_VAR,
        GET_VAR,
        SET_VAR,
        ARR_GET,
        ARR_PUSH,
        ARR_POP,
        ARR_UNSEL,
        ARR_INDEX
    ]

    number [
        PUSH,
        GET,
        SET
    ]

    manual [
        GET_VAR_OFFSET,
        SET_VAR_OFFSET,
        MOV,
        ARR_MAKE,
        WHILE,
        IF
    ]
);

fn parse_number<N: std::str::FromStr>(input: &str) -> IResult<&str, N>
where
    <N as std::str::FromStr>::Err: std::fmt::Debug,
{
    let is_number = digit1(input)?;
    // TODO: should I be error handling here?
    // Parsing the number should not be able to fail...
    let number = is_number.1.parse().unwrap();

    Ok((is_number.0, number))
}

#[test]
fn test_parse_number() {
    let test_data = "485";
    let (rem, res) = parse_number::<usize>(test_data).unwrap();

    assert_eq!(res, 485);
    assert!(rem.is_empty());
}

fn parse_operator(input: &str) -> IResult<&str, Instruction> {
    let parse_div = map(char('/'), |_| Instruction::DIV8);
    let parse_mul = map(char('*'), |_| Instruction::MUL8);
    let parse_plus = map(char('+'), |_| Instruction::ADD8);
    let parse_min = map(char('-'), |_| Instruction::SUB8);
    let parse_mod = map(char('%'), |_| Instruction::MOD8);

    alt((parse_div, parse_mul, parse_plus, parse_min, parse_mod))(input)
}

fn parse_compact(input: &str) -> IResult<&str, Instruction> {
    let compact_push = map(parse_number, |n| Instruction::PUSH(n));

    alt((compact_push, parse_operator))(input)
}

fn parse_comment(input: &str) -> IResult<&str, Option<Instruction>> {
    // Peek is here so the newline remains in the output, thereby being detected by the `plus_ws!` scattered everywhere.
    let parser = tuple((tag("//"), not_line_ending, peek(line_ending)));

    let (rem, _) = parser(input)?;

    Ok((rem, None))
}

/// Handle written instructions AND compact forms like `+` or `10`
fn parse_statement(input: &str) -> IResult<&str, Option<Instruction>> {
    let wrap_in_some = |a| Some(a);
    alt((
        parse_comment,
        map(parse_compact, wrap_in_some),
        map(parse_instr, wrap_in_some),
    ))(input)
}

fn parse_sequence_of_statements(input: &str) -> IResult<&str, Vec<Instruction>> {
    fold_many0(plus_ws!(parse_statement), Vec::new(), |mut acc, i| {
        if let Some(instr) = i {
            acc.push(instr);
        }

        acc
    })(input)
}

fn parse_block(input: &str) -> IResult<&str, Vec<Instruction>> {
    let begin = plus_ws!(tag("{"));
    let end = tag("}");

    delimited(begin, parse_sequence_of_statements, end)(input)
}

#[test]
fn test_parse_block() {
    let test_data = "{
            PUSH 50
            PUSH 40
            ADD8

            SET_VAR sum
        }";
    let (rem, res) = parse_block(test_data).unwrap();

    assert_eq!(
        res,
        vec!(
            Instruction::PUSH(50),
            Instruction::PUSH(40),
            Instruction::ADD8,
            Instruction::SET_VAR("sum".to_string())
        )
    );
    assert!(rem.is_empty());
}

pub fn parse_program(
    input: &str,
) -> Result<Vec<Instruction>, nom::Err<(&str, nom::error::ErrorKind)>> {
    let parser = preceded(multispace0, parse_sequence_of_statements);
    let maybe_parse_statement = opt(parse_statement);

    let (_, (mut res, maybe_instr)) = all_consuming(tuple((parser, maybe_parse_statement)))(input)?;

    if let Some(Some(instr)) = maybe_instr {
        res.push(instr);
    }

    Ok(res)
}

#[test]
fn test_parse_arr_destroy() {
    let test_data = "ARR_DESTROY";
    let (rem, res) = ARR_DESTROY(test_data).unwrap();

    assert_eq!(res, Instruction::ARR_DESTROY);
    assert!(rem.is_empty());
}

#[test]
fn test_parse_arr_index() {
    let test_data = "ARR_INDEX testvector";
    let (rem, res) = ARR_INDEX(test_data).unwrap();

    assert_eq!(res, Instruction::ARR_INDEX("testvector".to_string()));
    assert!(rem.is_empty());
}
