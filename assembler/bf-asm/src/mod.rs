mod compiler;
mod environment;
mod instructions;
mod nom_parser;
mod reader;

pub use compiler::Compiler;
pub use environment::Environment;
pub use instructions::Instruction;
pub use nom_parser::parse_program;

extern crate bf_interpreter;

#[test]
fn test_simple_program() {
    let mut output = Vec::new();
    let mut env = environment::Environment::new(std::io::empty(), &mut output);
    let test = "
        10
        8 8 *
        1 +
        PUT
        POP
        PUT
    ";

    let program = parse_program(test).unwrap();

    env.execute_program(&program);
    assert_eq!(b"A\n", output.as_slice());

    run_also_in_brainfuck_error_reader(&program).unwrap();
}

pub fn run_also_in_brainfuck<R: std::io::Read>(
    program: &[Instruction],
    reader: R,
) -> std::io::Result<()> {
    use reader::ReaderFactory;

    let mut readers = ReaderFactory::new(reader);
    let mut n_output = Vec::new();
    let mut bf_output = Vec::new();
    let mut n_env = environment::Environment::new(readers.new_reader(), &mut n_output);
    let mut bf_env = bf_interpreter::BFEnvironment::new(readers.new_reader(), &mut bf_output);
    let mut compiler = Compiler::new();

    for i in program {
        test_brainfuck(i, &mut compiler, &mut n_env, &mut bf_env)?;
    }
    Ok(())
}

pub fn run_also_in_brainfuck_error_reader(program: &[Instruction]) -> std::io::Result<()> {
    run_also_in_brainfuck(program, reader::ErrorReader)
}

pub fn run_also_in_brainfuck_stdin(program: &[Instruction]) {
    run_also_in_brainfuck(program, std::io::stdin()).unwrap();
}

fn test_brainfuck<
    R: std::io::Read,
    W: std::io::Write + std::fmt::Debug + PartialEq<T>,
    S: std::io::Read,
    T: std::io::Write + std::fmt::Debug,
>(
    instr: &Instruction,
    compiler: &mut Compiler,
    n_env: &mut environment::Environment<S, T>,
    bf_env: &mut bf_interpreter::BFEnvironment<R, W>,
) -> std::io::Result<()> {
    // execute in both environments
    instr.execute(n_env)?;

    let bf_text = instr.get_brainfuck(compiler);
    let bf_code = bf_interpreter::parse_brainfuck(&bf_text).unwrap();
    bf_code.into_iter().for_each(|bf_i| bf_i.execute(bf_env));

    // compare both environments
    assert_eq!(
        n_env, bf_env,
        "Environments uneven after executing {:?}.",
        instr
    );

    Ok(())
}
