pub mod state;
pub mod vars;

use super::Instruction;
use vars::NameHolder;
use state::{StackDescriptor, StackElem, StackTracker};

#[derive(Clone)]
pub struct Compiler {
    pub vars: vars::NameHolder,
    pub stack_height: StackDescriptor,
    compiled_brainfuck: String,
}

impl Compiler {
    pub fn new() -> Compiler {
        Compiler {
            vars: NameHolder::empty(),
            stack_height: StackDescriptor::new(vec![StackElem::SizedElem(1)]),
            compiled_brainfuck: String::new(),
        }
    }

    pub fn compile(&mut self, code: &[Instruction]) {
        let temp = &self.compile_scope(code);
        self.compiled_brainfuck += temp;
    }

    pub fn compile_scope(&mut self, code: &[Instruction]) -> String {
        // Enter scope
        self.vars.new_scope();

        // Accumulator for brainfuck
        let mut comp = String::new();

        // Compile all instructions
        for i in code {
            comp.push_str(&i.get_brainfuck(self));
        }

        // Leave scope again
        self.vars.exit_scope();

        // Return accumulator
        comp
    }

    pub fn dense(&self) -> &str {
        &self.compiled_brainfuck
    }
}
