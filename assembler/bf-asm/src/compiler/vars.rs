use super::{StackDescriptor, StackElem, StackTracker};
use std::collections::HashMap;

#[derive(Clone)]
pub struct NameHolder {
    scopes: Vec<HashMap<String, StackDescriptor>>,
}

impl NameHolder {
    pub fn empty() -> Self {
        NameHolder {
            scopes: vec!(HashMap::new())
        }
    }

    pub fn new_scope(&mut self) {
        self.scopes.push(HashMap::new());
    }

    pub fn exit_scope(&mut self) {
        self.scopes.pop();
    }

    pub fn declare_var(&mut self, name: String, stack_height: &StackDescriptor) {
        // This permits shadowing (reusing) names
        self.scopes.last_mut().unwrap().insert(name, stack_height.clone());
    }

    pub fn get_height_var(&self, name: &String) -> &StackDescriptor {
        for s in self.scopes.iter().rev() {
            if let Some(ind) = s.get(name) {
                return ind;
            }
        }

        panic!("Unknown variable `{}`, did you declare it?", name);
    }

    pub fn get_vector_var(&self, name: &String) -> &super::super::instructions::vector::BFVector {
        let height = self.get_height_var(name);

        if let StackElem::Vector(vec) = height.get_elems().last().unwrap() {
            vec
        } else {
            panic!("{} is not a vector", name)
        }
    }
}