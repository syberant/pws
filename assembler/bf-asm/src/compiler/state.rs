use super::super::instructions::vector::BFVector;

pub trait StackTracker: Sized {
    fn new(elems: Vec<StackElem>) -> Self;

    fn get_elems(&self) -> &[StackElem];

    fn move_to(&self, rhs: &Self) -> String {
        let stack = self.complement(rhs);
        let mut acc = String::new();

        for el in stack.get_elems().iter().rev() {
            let move_instr = el.move_left();
            acc.push_str(&move_instr);
        }

        return acc;
    }

    fn move_back_from(&self, rhs: &Self) -> String {
        let stack = self.complement(rhs);
        let mut acc = String::new();

        for el in stack.get_elems() {
            let move_instr = el.move_right();
            acc.push_str(&move_instr);
        }

        return acc;
    }

    fn pop(&mut self, elem: StackElem);

    fn push(&mut self, elem: StackElem);

    fn push_sized(&mut self, n: usize) {
        let el = StackElem::SizedElem(n);
        self.push(el);
    }

    fn pop_sized(&mut self, n: usize) {
        let el = StackElem::SizedElem(n);
        self.pop(el);
    }

    /// Get the complement, i.e. the top part of the stack that's different from the stack we're comparing against
    fn complement(&self, rhs: &Self) -> Self;

    // Following are some convenient methods
    fn add_stack_height(&mut self, n: usize) {
        self.push_sized(n);
    }

    fn sub_stack_height(&mut self, n: usize) {
        self.pop_sized(n);
    }

    fn add_vector_stack_height(&mut self, vector: super::super::instructions::vector::BFVector) {
        self.push(StackElem::Vector(vector));
    }

    fn sub_vector_stack_height(&mut self, vector: super::super::instructions::vector::BFVector) {
        self.pop(StackElem::Vector(vector));
    }
}

#[derive(Clone, PartialEq)]
pub struct StackDescriptor {
    elems: Vec<StackElem>,
}

impl StackTracker for StackDescriptor {
    fn new(elems: Vec<StackElem>) -> StackDescriptor {
        StackDescriptor { elems }
    }

    fn get_elems(&self) -> &[StackElem] {
        &self.elems
    }

    fn pop(&mut self, elem: StackElem) {
        let top = self.elems.pop().unwrap();
        let maybe_new_top = top.remove_from_this(&elem).unwrap();

        if let Some(new_top) = maybe_new_top {
            self.elems.push(new_top);
        }
    }

    fn push(&mut self, elem: StackElem) {
        if let Some(new_top) = self.elems.last_mut().unwrap().combine(elem) {
            self.elems.push(new_top);
        }
    }

    fn complement(&self, rhs: &StackDescriptor) -> StackDescriptor {
        let mut acc_vec = Vec::new();

        let rhs_last_index = rhs.elems.len() - 1;
        let self_elem = &self.elems[rhs_last_index];
        let rhs_last = &rhs.elems[rhs_last_index];
        let maybe_item = self_elem
            .remove_from_this(rhs_last)
            .expect("Conflicting elements, these are not views of the same stack.");

        if let Some(i) = maybe_item {
            acc_vec.push(i);
        }

        let rest = self.elems.get(rhs_last_index + 1..);

        if let Some(i) = rest {
            acc_vec.extend_from_slice(i);
        }

        return StackDescriptor { elems: acc_vec };
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum StackElem {
    SizedElem(usize),
    Vector(BFVector),
}

impl StackElem {
    pub fn is_sized(&self) -> bool {
        use StackElem::*;

        match self {
            SizedElem(..) => true,
            Vector(..) => false,
        }
    }

    pub fn move_left(&self) -> String {
        use StackElem::*;

        match self {
            SizedElem(n) => "<".repeat(*n),
            Vector(v) => v.from_end_to_begin(),
        }
    }

    pub fn move_right(&self) -> String {
        use StackElem::*;

        match self {
            SizedElem(n) => ">".repeat(*n),
            Vector(v) => v.from_begin_to_end(),
        }
    }

    pub fn remove_from_this(&self, rhs: &StackElem) -> Result<Option<StackElem>, String> {
        use StackElem::*;

        if *rhs == SizedElem(0) {
            // Empty SizedElem, always succeeds and changes nothing
            return Ok(Some(self.clone()));
        }

        match self {
            Vector(v) => match rhs {
                Vector(v_rhs) => {
                    // Both are vectors

                    if v != v_rhs {
                        Err("Vectors are not the same.".to_string())
                    } else {
                        // Nothing is left
                        Ok(None)
                    }
                }
                other => Err(format!(
                    "Can't remove non-vector `{:?}` from vector `{:?}`.",
                    other, v
                )),
            },
            SizedElem(n) => match rhs {
                SizedElem(n_rhs) => {
                    // Both are sized objects

                    if n_rhs > n {
                        Err("Can't remove bigger amount than there is.".to_string())
                    } else if n_rhs == n {
                        Ok(None)
                    } else {
                        Ok(Some(SizedElem(n - n_rhs)))
                    }
                }
                _ => Err("Can't remove non-sized from sized.".to_string()),
            },
        }
    }

    pub fn combine(&mut self, rhs: StackElem) -> Option<StackElem> {
        use StackElem::*;

        match self {
            Vector(_v) => Some(rhs),
            SizedElem(n) => match rhs {
                SizedElem(n_rhs) => {
                    *n += n_rhs;
                    None
                }
                _ => Some(rhs),
            },
        }
    }
}
