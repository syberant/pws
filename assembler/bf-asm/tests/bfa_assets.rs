extern crate bf_asm;

#[test]
fn test_all_bfa_assets() {
    use std::fs::{File, read_dir};

    let all_assets = read_dir("../assets")
        .unwrap_or_else(|_|
            read_dir("./assets")
                .expect("Could not find assets folder, are you in the right directory?")
        );

    let mut error_counter = 0;
    
    for res_filename in all_assets {
        let filename = res_filename.unwrap();
        let path = filename.path();
        let maybe_type = path.extension();

        if let Some(filetype) = maybe_type {
            if filetype == "bfa" {
                use std::io::Read;

                println!("Testing {}.", path.display());
                let file = File::open(path).expect("Couldn't open file for some reason...");


                let mut reader = std::io::BufReader::new(file);
                let mut contents = String::new();
                reader
                    .read_to_string(&mut contents)
                    .expect("Couldn't read file.");
                
                let prog = bf_asm::parse_program(&contents).unwrap();
                let res = bf_asm::run_also_in_brainfuck_error_reader(&prog);
                if res.is_err() {
                    error_counter += 1;
                }
            }
        }
    }

    println!("\nCounted {} programs who tried to read.", error_counter);
}