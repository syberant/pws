extern crate bf_asm;
extern crate bf_interpreter;
extern crate clap;

fn main() {
    use clap::{App, Arg, SubCommand};

    let matches = App::new("Brainf*ck tools - CLI")
        .subcommand(
            SubCommand::with_name("bf")
                .about("Functionality related to brainfuck")
                .arg(
                    Arg::with_name("run")
                        .short("r")
                        .long("run")
                        .help("Run the program")
                        .takes_value(false),
                )
                .arg(
                    Arg::with_name("compile")
                        .short("c")
                        .long("compile")
                        .help("Compile the program to linux x86_64 assembly")
                        .takes_value(false)
                        .conflicts_with("run"),
                ),
        )
        .subcommand(
            SubCommand::with_name("bfa")
                .about("Functionality related to the assembly compilable to brainfuck")
                .arg(
                    Arg::with_name("run")
                        .short("r")
                        .long("run")
                        .help("Run the program")
                        .takes_value(false),
                )
                .arg(
                    Arg::with_name("compile")
                        .short("c")
                        .long("compile")
                        .takes_value(false)
                        .help("Compile the program to brainfuck")
                        .conflicts_with("run"),
                )
                .arg(
                    Arg::with_name("readable")
                        .short("n")
                        .long("readable")
                        .takes_value(false)
                        .help("Make the compiled code somewhat more human-readable")
                        .requires("compile"),
                )
                .arg(
                    Arg::with_name("test")
                        .short("t")
                        .long("test")
                        .takes_value(false)
                        .help("Test code by running its brainfuck equivalent as well")
                        .conflicts_with("compile")
                        .conflicts_with("run"),
                ),
        )
        .arg(
            Arg::with_name("source")
                .short("s")
                .long("source")
                .value_name("FILE")
                .help("Takes the file you want to compile.")
                .takes_value(true)
                .required(true),
        )
        .get_matches();

    let filename = std::path::Path::new(matches.value_of("source").unwrap());

    // TODO: get language from file extension?

    use std::io::Read;

    let file = std::fs::File::open(filename).expect("Couldn't open file.");
    let mut reader = std::io::BufReader::new(file);
    let mut contents = String::new();
    reader
        .read_to_string(&mut contents)
        .expect("Couldn't read file.");

    match matches.subcommand() {
        ("bf", Some(m)) => {
            use bf_interpreter::*;

            if m.is_present("run") {
                // run the file

                let prog = parse_brainfuck(&contents).unwrap();
                let mut env = BFEnvironment::new(std::io::stdin(), std::io::stdout());
                for i in prog {
                    i.execute(&mut env);
                }
            } else if m.is_present("compile") {
                // compile the file

                let prog = parse_brainfuck(&contents).unwrap();
                let compiler = Compiler::new(prog);
                println!("{}", compiler.compile());
            }
        }
        ("bfa", Some(m)) => {
            use bf_asm::*;

            if m.is_present("run") {
                // run the file
                let prog = parse_program(&contents).unwrap();
                let mut env = Environment::new(std::io::stdin(), std::io::stdout());

                for i in prog {
                    i.execute(&mut env).unwrap();
                }
            } else if m.is_present("compile") {
                // compile to brainfuck
                let prog = parse_program(&contents).unwrap();

                if m.is_present("readable") {
                    unimplemented!();
                } else {
                    let mut compiler = Compiler::new();
                    compiler.compile(&prog);

                    print!("{}\n", compiler.dense());
                }
            } else if m.is_present("test") {
                // test by running the brainfuck equivalent as well
                let prog = parse_program(&contents).unwrap();

                run_also_in_brainfuck_stdin(&prog);
            }
        }
        _ => unreachable!(),
    }
}
